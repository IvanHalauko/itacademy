﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191012ExercizeCollectionPart1
{
	public class WhiteBox : IBox<int>
	{  //Реализуем интерфейс
		public int IsClosed => 1;

		//private bool IsClosed { get { return false; } } //

		public int IsOpened()
		{
			return GetResult();
		}

		public int GetResult()
		{
			return 0;
		}


	}
}
