﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191012ExercizeCollectionPart1
{
	public class BlackBox : IBox<bool>
	{  //Реализуем интерфейс
		public bool IsClosed => false;

		//private bool IsClosed { get { return false; } } //

		public bool IsOpened()
		{
			return true;
		}
	}
}
