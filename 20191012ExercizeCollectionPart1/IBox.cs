﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191012ExercizeCollectionPart1
{
	public interface IBox<T>
	{
		T IsOpened();
		//Содержит только сигнатуру
		T IsClosed { get; }

	}
}
