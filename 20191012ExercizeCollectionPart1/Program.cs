﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191012ExercizeCollectionPart1
{
	class Program
	{
		static void Main(string[] args)
		{
			IBox<bool> blackBox = new BlackBox();
			WhiteBox WhiteBox = new WhiteBox();

			IBox<int> iWhiteBox = WhiteBox;

			List<int> listInt = new List<int>(); //Пустая коллекция типа Int
			List<int> listInt1 = new List<int>(10); //Пустая коллекция типа Int на 10 элементов
			List<int> listInt2 = new List<int>(4) { 1,2,3,4}; //коллекция типа Int на 4 элемента со значениями
			List<int> listInt3 = new List<int> {1,2,3,4,5}; //коллекция типа Int со значениями

			Console.WriteLine("Добавляем элемент 12 и выводим список List<int> listInt = new List<int>(); ");
			listInt.Add(12);
			foreach (int elem in listInt)
			{
				Console.WriteLine(elem);
			}


			Console.WriteLine("Вывод элементов листа List<int> listInt2 = new List<int>(4) { 1,2,3,4};");
			foreach (int elem in listInt2)
			{
				Console.WriteLine(elem);				
			}
			Console.WriteLine(" ");

			Console.WriteLine("удаляем третий элемент листа listInt2");
			listInt2.RemoveAt(3);
			foreach (int elem in listInt2)
			{
				Console.WriteLine(elem);
			}
			Console.WriteLine(" ");

			Console.WriteLine("Выводим второй элемент листа: listInt2");
			listInt2.ForEach(c=> Console.WriteLine(listInt2[1]));

			Console.WriteLine("Создаем словарь");
			Dictionary<int, string> dictionary = new Dictionary<int, string>();
			dictionary.Add(1, "value1");
			dictionary.Add(2, "value2");
			Console.WriteLine("Добавляем элементы в словарь коммандой Add");
			//foreach (int elem in dictionary)
			//{
			//	Console.WriteLine(elem);
			//}

			Console.WriteLine("выводим значение из словаря по ключу 1");
			Console.WriteLine(dictionary[1]);
			Console.WriteLine("выводим значение из словаря по ключу 2");
			Console.WriteLine(dictionary[2]);

			Console.WriteLine(dictionary.TryGetValue(7,out var s)? s:"null"); //это работает всегда

			/////Второй способ задания словаря
			if (dictionary.TryGetValue(7,out var sn))
			{
				//
			}
			else
			{
				dictionary.Add(7, "null");
			}

			//Класс Queue<T> представляет обычную очередь, работающую по 
			//алгоритму FIFO ("первый вошел - первый вышел").

			//У класса Queue<T> можно отметить следующие методы:
			//Dequeue: извлекает и возвращает первый элемент очереди
			//Enqueue: добавляет элемент в конец очереди
			//Peek: просто возвращает первый элемент из начала очереди без его удаления
			Console.WriteLine("Инициализируем список при помощи Queue<T>");

			Queue<int> numbers = new Queue<int>();
			//добавляем элементы в список Queue
			numbers.Enqueue(1);
			numbers.Enqueue(2);
			numbers.Enqueue(3);
			numbers.Enqueue(4);
			Console.WriteLine(numbers.Peek());
			Console.WriteLine(numbers.Dequeue()); //получаем(Dequeue) первый элемент очереди и его удаляет
												  //и выводим на консоль первый элемент списка

			int firstElement = numbers.Dequeue(); // получаем (Dequeue) первый элемент очереди и его удаляет
			Console.WriteLine(firstElement); // выводим на консоль первый элемент списка

			foreach (var item in numbers)
			{
				Console.WriteLine($"выводим элемент коллекции numbers вида Queue: {item}");
			}
			numbers.Clear();
			for (int i = 0; i < 5; i++)
			{
				numbers.Enqueue(i);
				Console.WriteLine(numbers);
			}

		}
	}
}
