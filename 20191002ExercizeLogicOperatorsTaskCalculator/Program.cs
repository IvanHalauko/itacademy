﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{ //простой калькулятор ввод значений в переменную из консоли
 public	class Program
	{
		static void Main(string[] args)
		{
			int x = 0;
			int y = 0;

			Console.WriteLine("Support operations: -,/,* ");
			Console.WriteLine("Enter value for X:");
			//x=Console.ReadLine
			int.TryParse(Console.ReadLine(), out x);

			Console.WriteLine("Enter value for Y:");
			int.TryParse(Console.ReadLine(), out y);

			Console.WriteLine("Enter operation:");
			var operation = Console.ReadKey();

			Console.WriteLine($"Result:{Calculate(x, y, operation)}");
			Console.ReadLine();
		}
		static int Calculate(int x, int y, ConsoleKeyInfo operation)
		{
			if (operation.Key == ConsoleKey.OemPlus)
			{
				return x + y;
			}

			if (operation.Key == ConsoleKey.OemMinus)
			{
				return x - y;
			}

			if (operation.Key == ConsoleKey.Oem2)
			{
				return x / y;
			}
			if (operation.Key == ConsoleKey.D8)
			{
				return x * y;
			}

			return -1;

		}
	}
}
