﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191009ExercizeArrayForeachTutor
{
	class ForEachTutorProgram
	{

		static void Main(string[] args)
		{
			int[] i = new int[6];
			Array.ForEach(i, b => int.TryParse(Console.ReadLine(), out b));

			for (int y=0; y< i.Length; y++)
			{
				Console.WriteLine($"array[{y}]={i[y]}");
			}
		}
	}
}
