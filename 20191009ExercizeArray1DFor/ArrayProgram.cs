﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191009Exercize
{
	class ArrayProgram
	{   //Объявить массив int на 6 элементов.
		//Ввести значения элементов массива из консоли.
		//Вывести введенные значения на экран

		static void Main(string[] args)
		{
			int sizeArray = 0;
			Console.WriteLine("Введите размерность массива");

			int.TryParse(Console.ReadLine(), out sizeArray); //Объявить массив int на 6 элементов, сделал путем 
															//ввода размерности из консоли вручную
			int[] data_1 = new int[sizeArray];

			for (int i = 0; i < sizeArray; i++)
			{
				int.TryParse(Console.ReadLine(), out data_1[i]); //Ввести значения элементов массива из консоли.
			}

			for (int i = 0; i < sizeArray; i++)
			{
				Console.WriteLine(data_1[i]); //Вывести введенные значения на экран
			}
		}
	}
}
