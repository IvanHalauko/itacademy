﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _20191127Exercize_WinFORM_Asinc_Await_Task
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private async void button1_Click(object sender, EventArgs e)
		{
			button1.Enabled = false;

			HttpClient httpClient = new HttpClient(); //(2)

			toolStripStatusLabel2.Text = "Getting HTML....."; // 3
			var habrResult1 = await httpClient.GetStringAsync("https://habr.com/ru/post/260217/"); //4
			var habrResult2 = await httpClient.GetStringAsync("https://habr.com/ru/news/");
			var habrResult3 = await httpClient.GetStringAsync("https://habr.com/ru/");
			var habrResult4 = await httpClient.GetStringAsync("https://habr.com/ru/hubs/");

			toolStripStatusLabel2.Text = "Saving process...."; //5
			string fileName = $"{Guid.NewGuid()}.html";

			//if (habrResult1.)
			//{

			//}
			Task saveFile = Task.Run(() =>
			{
				File.WriteAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName), habrResult1); //6
			});
			await saveFile; // 7

			listBox1.Items.Add(fileName); //8

			toolStripStatusLabel2.Text = "Create file" + fileName; //9
		}

		private async void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (listBox1.Items.Count > 0 && listBox1.SelectedItem != null)
			{
				var filename = listBox1.SelectedItem as string;
				string[] lines = null;

				Task readFile = Task.Run(() =>
				{
					lines = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filename));


				}).ContinueWith(c =>
				{
					foreach (var line in lines)
					{
						listBox2.Items.Add(line);
					}

				}, TaskScheduler.FromCurrentSynchronizationContext());
				await readFile;

				toolStripStatusLabel2.Text = "File was readed";
			}
		}



	}
}
