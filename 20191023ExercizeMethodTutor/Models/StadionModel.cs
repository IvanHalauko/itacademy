﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191023ExercizeMethodTutor.Models
{   //4. Создать класс Stadion(Name, Sectors List<Sector>)
	class StadionModel
	{
		public int Name { get; set; }

		public List<Sector> Sectors {get; set;}
		// создаем 10 секторов

		public StadionModel()
		{
			//наполняем коллекцию 4-ю секторами
			Sectors = new List<Sector>();

			for (int i = 1; i < 5; i++)
			{
				// Заполняем коллекцию
				Sectors.Add(new Sector());
			}
		}



	}
}
