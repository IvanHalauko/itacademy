﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191023ExercizeMethodTutor.Models
{
	class Row
	{
		//2. Создать класс Row(Places list<Place>)
		public Row()
		{
			//наполняем коллекцию 5 ю случайными местами
			Places = new List<Place>();
			var random = new Random();

			// Заполняем коллекцию 
			for (int i = 5; i > 0; i--)
			{
				Places.Add(new Place(i, random.Next(50, 100)));
			}
		}

		public List<Place> Places { get; set; }
		//Создаем метод печати свободных мест
		public void PrintFreePlaces()
		{
			foreach (var item in Places)
			{
				if (!item.IsReserved)
				{
					Console.WriteLine($"это место № {item.Number} свободно ");
				}
			}
		}
		
	}
}
