﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191009ExercizeArray2DFor
{       //Создать двумерный массив 3х3. Значения элементов массива инициализировать при объявлении массива. 
		//Вывести на экран значения максимального элемента каждого ряда

	class Array2DMaxProgram
	{
		static void Main()
		{
			int sizeArr = 3;
			int[,] twoDimensArray = new int [3, 3] {{ 1, 4, 5 },{ 1,2,3},{5,7,9}}; //Создать двумерный массив 3х3. Значения элементов 
																				  //массива инициализировать при объявлении массива.
			for (int rowIndex = 0; rowIndex < sizeArr; rowIndex++)
			{
				int[] row = Enumerable.Range(0, twoDimensArray.GetUpperBound(1) + 1).Select(i => twoDimensArray[rowIndex, i]).ToArray(); //Выборка строки из двумерного массива и передача в одномерный массив
				var maxValue = row.Max();                                   //Поиск максимального элемента в двумерном массиве
				Console.Write($" массив:{rowIndex} элементы массива:");
				foreach (var element in row)
				{
					Console.Write(element);
					Console.Write(" ");
				}
				Console.WriteLine($"максимальное значение массива: {maxValue}");
			}			
		}
	}
}
