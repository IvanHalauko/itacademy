﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlusHuman
{
	public class Human
	{
		public string Name { get; set; }


		public static Human operator +(Human man, Human woman)
		{
			return new Human { Name = $"Child father: {man.Name}, mother {woman.Name}" };
		}
	}
}