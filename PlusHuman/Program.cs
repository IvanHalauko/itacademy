﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlusHuman
{//https://metanit.com/sharp/tutorial/3.36.php
	class Program
	{
		//Перегрузка операторов
		static void Main(string[] args)
		{
			var man = new Men { Name = "Uvasya" };
			var woman = new Woman { Name = "Uvasilisa" };

			var human = man + woman;
			Console.WriteLine(human.Name);
		}
	}
}
