﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191005ExerciseWriteAllNumbDevide3
{
	class Program
	{
		static void Main(string[] args)
		{
			int x = 0;
			Console.WriteLine("Enter value for X:");
			//x=Console.ReadLine
			int.TryParse(Console.ReadLine(), out x);

			for (int i = 0; i < x; i++)
				if (i%3 == 0)
					Console.WriteLine($"Делится на 3: {i}");
					   
		}
	}
}
