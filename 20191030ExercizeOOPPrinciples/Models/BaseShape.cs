﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191030ExercizeOOPPrinciples.Models
{//1
	public class BaseShape
	{
		//Определяет общие свойства
		public int X { get; set; }
		public int Y { get; set; }

		public virtual double Square()
		{
			return X * Y;
		}


	}
}
