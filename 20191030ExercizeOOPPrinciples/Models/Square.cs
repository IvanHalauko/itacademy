﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191030ExercizeOOPPrinciples.Models
{
	public class Squared : Rectangle
	{
		public Squared(int width, int height, int diag)
			: base(width, height, diag)   //!!!!!
		{
			X = width;
			Y = height;
			C = diag;
		}
		public override double Square()
		{
			return base.Square();
		}
		public override double Perimetr()
		{
			return base.Perimetr();
		}


	}
}
