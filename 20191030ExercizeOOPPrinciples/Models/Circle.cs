﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191030ExercizeOOPPrinciples.Models
{//2
	public class Circle : BaseShape
	{
		public Circle(int radius)
		{
			X = radius;			
		}
		public override double Square()
		{
			return 2 * Math.PI * Math.Sqrt(X);
		}
	}
}
