﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191030ExercizeOOPPrinciples.Models
{
	public class Rectangle : BaseShape
	{
		public int C { get; set; }

		public Rectangle(int width, int height, int diagonal)
		{

			X = width;
			Y = height;
			C = diagonal;
		}

		public override double Square()
		{
			return base.Square();
		}

		public virtual double Perimetr()
		{
			return X + X + Y + Y;
		}

	}
}
