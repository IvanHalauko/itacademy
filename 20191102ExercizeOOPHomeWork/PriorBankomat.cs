﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    public class PriorBankomat : Bankomat
    {
		// При снятии денег с карты Belarusbank комиссия не снимается ни в одном банке
		// При снятии денег с карты Priorbank комиссия не снимается в Приор и Беларус в MT 1%
		// При снятии денег с карты MTBank комиссия не снимается в MTBank в Беларус 1% в Приор 10%
		public override double GetCash(CreditCard creditCard, double takeOff)
        {
			double value = creditCard.Value;
			double newValueCard = 0;
			var typeBankCard = creditCard.TypeBankCard;

			
			{

				if (typeBankCard == TypeBank.Belarusbank)
				{
					return newValueCard = value - takeOff;
				}
				if (typeBankCard == TypeBank.Priorbank)
				{
					return newValueCard = value - takeOff;
				}
				if (typeBankCard == TypeBank.MTBank)
				{
					newValueCard = value - takeOff * 1.1;
					return newValueCard;
				}
			}
			return -1;

		}
	}
}
