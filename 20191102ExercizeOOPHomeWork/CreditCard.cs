﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
	//0. (выполнено) Создать класс CreditCard (Id, TypeBanks, Value )
	public class CreditCard
    {
        public int ID { get; set; }
        public TypeBank TypeBankCard { get; set; }
        public double Value { get; set; }

		/// <summary>
		/// dfgdgd
		/// </summary>
		/// <param name="id"></param>
		/// <param карта банка="cardTypeBank">карта банка</param>
		/// <param остаток на счете="value">остаток на счете</param>
		public CreditCard(int id, TypeBank typeBankCard, double value)
		{
			ID = id;
			Value =value;
			TypeBankCard = typeBankCard;
			//return _value;
		}
		public override string ToString()
		{

			return string.Format("У Вас есть карта №: {0}; банка: {1}; с суммой на счете: {2};", ID, TypeBankCard, Value);
		}
	}
}
