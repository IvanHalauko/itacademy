﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Enum TypeBanks { Belarusbank, Priorbank, MTBank }
//0. (выполнено) Создать класс CreditCard (Id, TypeBanks, Value )
//1. (выполнено) Создать абстрактный класс банкомат (double GetCash(CreditCard, count))
//2. (выполнено) Создать 3 классса банкоматов (Belarusbank, Priorbank, MTBank)
//3. (выполнено) При снятии денег с карты Belarusbank комиссия не снимается ни в одном банке
//4. (выполнено) При снятии денег с карты Priorbank комиссия не снимается в Приор и Беларус в MT 1%
//5. (выполнено) При снятии денег с карты MTBank комиссия не снимается в MTBank в Беларус 1% в Приор 10%
//6. (выполнено) Если на карте не достаточно средств, то выводить сообщение
//7. (выполнено, в коде) Создать по одной карте каждого банка (в коде или с консоли)
//8. (выполнено) Как пользователь я хочу выбрать карту, после выбрать банкомат, и ввести сумму для снятия
//9. (выполнено) После снятия либо сообщение о недостатке средств, либо сообщение об снятой сумме и сумме на остатке

namespace HomeWork
{
    public class Program
    {
        static void Main(string[] args)
        {
			//7. (выполнено, в коде) Создать по одной карте каждого банка (в коде или с консоли)
			CreditCard kard1Belarus = new CreditCard(1,TypeBank.Belarusbank, 3000);
			CreditCard kard2Prior = new CreditCard(2, TypeBank.Priorbank, 2000);
			CreditCard kard3MT = new CreditCard(3,TypeBank.MTBank,1000);
						
			List<CreditCard> listCards = new List<CreditCard>();
			listCards.Add(kard1Belarus);
			listCards.Add(kard2Prior);
			listCards.Add(kard3MT);
			
			BelarusBankomat belarusBankomat = new BelarusBankomat();
			MTBankomat mtBankomat = new MTBankomat();
			PriorBankomat priorBankomat = new PriorBankomat();

			//8. (выполнено) Как пользователь я хочу выбрать карту, после выбрать банкомат, и ввести сумму для снятия
			//Console.WriteLine("У вас в кошельке следующие банковские карты: 1-kard1Belarus, 2-kard2Prior, 3- kard3MT");
			//выводим список всех кредитных карт
			DisplayToConsole(listCards);
			Console.WriteLine("Выберите номер карты, которой хотите воспользоваться");
			
			var cardKey = Console.ReadKey();
			Console.WriteLine("");
			var card = listCards[1];
			switch (cardKey.Key)
			//(consoleKey.Key)
			{
				case (ConsoleKey.D1):
					card = listCards[0];
					break;
					
				case (ConsoleKey.D2):
					card = listCards[1];
					break;
				case (ConsoleKey.D3):
					card = listCards[2];
					break;
			}
			Console.WriteLine("Выберите банкомат с которого Вы хотите снять наличные.");
			Console.WriteLine("Нажмите клавишу b - belarusBankomat, m - mtBankomat, p - priorBankomat");
			Console.WriteLine("");
			var bank = Console.ReadKey();
			Console.WriteLine("");
			Console.WriteLine("Какую сумму вы хотите снять? Введите сумму:");
			
			double.TryParse(Console.ReadLine(), out double takeOff);
			Console.WriteLine("");
			//6. (выполнено) Если на карте не достаточно средств, то выводить сообщение
			if (takeOff<card.Value)
			{
				switch (bank.Key)
				//(consoleKey.Key)
				{
					case (ConsoleKey.B):
						Console.WriteLine("Остаток на счете: " + belarusBankomat.GetCash(card, takeOff) + " Снята сумма " + takeOff);//Userы, снимающие деньги в беларусьбанкоматах
						return;
					case ConsoleKey.M:
						Console.WriteLine("Остаток на счете: " + mtBankomat.GetCash(card, takeOff) + " Снята сумма " + takeOff);//Userы, снимающие деньги в MTБанкомате
						return;
					case ConsoleKey.P:
						Console.WriteLine("Остаток на счете: " + priorBankomat.GetCash(card, takeOff) + " Снята сумма " + takeOff);//Userы, снимающие деньги в MTБанкомате
						return;
				}
			}
			else
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine("На карте не достаточно средств. Заберите карту!");
			}
							
		}

		public static void DisplayToConsole(List<CreditCard> listCards)
		{
			foreach (var item in listCards)
			{
				Console.WriteLine(item);
			}
		}

		//Это МУСОР, НО не удалять.
		//Console.WriteLine("Остаток на счете kard1Belarus:" + belarusBankomat.GetCash(card, takeOff) +"Снята сумма"+ takeOff);//Userы, снимающие деньги в беларусьбанкоматах
		//Console.WriteLine("Остаток на счете Пети:" + belarusBankomat.GetCash(kard2Prior, takeOff));//Userы, снимающие деньги в беларусьбанкоматах
		//Console.WriteLine("Остаток на счете Павла:" + belarusBankomat.GetCash(kard3MT, takeOff));//Userы, снимающие деньги в беларусьбанкоматах

		//Console.WriteLine("Остаток на счете Саши:" + mtBankomat.GetCash(kard1Belarus, takeOff));//Userы, снимающие деньги в MTБанкомате
		//Console.WriteLine("Остаток на счете Пети:" + mtBankomat.GetCash(kard2Prior, takeOff));//Userы, снимающие деньги в MTБанкомате
		//Console.WriteLine("Остаток на счете Павла:" + mtBankomat.GetCash(kard3MT, takeOff));//Userы, снимающие деньги в MTБанкомате


	}
}
