﻿namespace HomeWork
{
	//1. (выполнено) Создать абстрактный класс банкомат (double GetCash(CreditCard, count))
	public interface Bankomat
    {

		double takeOff { get; set; }
        double GetCash(CreditCard creditCard, double takeOff);

    }
}
