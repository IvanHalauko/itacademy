﻿using _20191023ExercizeMethodTutor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191023ExercizeMethodTutor
{		
		//5. Как пользователь, я хочу выбрать сектор(вводить с консоли,
		//при отсутствии сектора выводить сообщение), после выбора сектора 
		//я хочу получить список рядов в этом секторе где есть свободные 
		//места, после выбора ряда, получить список свободных мест, после 
		//выбора места, перевести его статус IsReserved в true, написать что 
		//есто забронировано и вернуть меня в первоначальное меню.

	public class Worker
	{
		private readonly StadionModel _stadion;

		public Worker()
		{
			_stadion = new StadionModel();
			Console.WriteLine("Сектора со свободными местами:");
			ShowAllSectors();
		}

		public void ShowAllSectors()
		{
			foreach (var item in _stadion.Sectors)
			{
				Console.WriteLine($"Сектор № {item.Name}");
			}
		}
		//Добавить метод который позволит получить список по номеру сектора
		public void ShowRowsInSectorWithFreePlaces(int sectorId)
		{
			var sector = _stadion.Sectors[sectorId];
			foreach (var item in sector.Rows)
			{
				item.PrintFreePlaces();
			}
		}

		public void ShowPlacesInRow(int sectorId,int rowId)
		{
			var sector = _stadion.Sectors[sectorId];
			var row = sector.Rows[rowId];

			row.PrintFreePlaces();
		}

		//метод который забронирует место

		public void BookPlace(int sectorId, int rowId, int placesId)
		{
			var sector = _stadion.Sectors[sectorId];
			var row = sector.Rows[rowId,sector];

			row.Places[placesId].IsReserved = true;
			row.PrintFreePlaces();
		}

	}
}
