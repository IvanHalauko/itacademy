﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191023ExercizeMethodTutor
{
	class Program
	{
		//1. Создать класс Place(Number, Price, IsReserved)
		//2. Создать класс Row(Places list<Place>)
		//3. Создать класс Sector(Name, Rows List<Row>)
		//4. Создать класс Stadion(Name, Sectors List<Sector>)
		//5. Как пользователь я хочу выбрать сектор(вводить с консоли,
		//при отсутствии сектора выводить сообщение), после выбора сектора 
		//я хочу получить список рядов в этом секторе где есть свободные 
		//места, после выбора ряда, получить список свободных мест, после 
		//выбора места, перевести его статус IsReserved в true, написать что 
		//есто забукано и вернуть меня в первоначальное меню.

		static void Main()
		{
			var worker = new Worker(); //рабочий терминал, обрабатывающий
			Console.WriteLine("Введите номер желаемого сектора");
			worker.ShowRowsInSectorWithFreePlaces(int.TryParse(Console.ReadLine(), out int sectorId) ? sectorId : 0);
			//Console.WriteLine("BBBB");
			worker.ShowPlacesInRow(sectorId, int.TryParse(Console.ReadLine(), out int rowId) ? rowId : 0);
			//Console.WriteLine("CCCCC");
			worker.BookPlace(sectorId, rowId, int.TryParse(Console.ReadLine(), out int placeId) ? placeId : 0);
		}


	}
}
