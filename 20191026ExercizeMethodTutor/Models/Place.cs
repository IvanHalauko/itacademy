﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191023ExercizeMethodTutor.Models
{   ////1. Создать класс Place и поля и свойства: Number, Price, IsReserved).
	public class Place
	{	
		public Place(int number, int price)
		{
			Number = number;
			Price = price;
		}

		public int Number { get; private set; }

		public int Price { get; private set; }

		//public bool IsReserved { get; set; }
		public StatePlace Status { get; set; } = StatePlace = free;
	}
}
