﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191023ExercizeMethodTutor.Models
{   //3. Создать класс Sector(Name, Rows List<Row>)
	class Sector
	{
		public string Name { get; set; }
		public List<Row> Rows { get; set; }
		public Sector()
		{
			//наполняем сектор 5-ю рядами
			Rows = new List<Row>();			
			for (int i = 1; i <5 ; i++)
			{
				Rows.Add(new Row());
				//Rows.Add(new Row(i,random.Next(1,10)));
			}
		}

	}
}
