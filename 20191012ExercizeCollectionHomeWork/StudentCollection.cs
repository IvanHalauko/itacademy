﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace _20191012ExercizeCollectionHomeWork
{
	class StudentCollection:Collection<Student>
	{
		public void InsertStudent(Student item)
		{
			if (item !=null)
			{
				base.Add(item);
				Console.WriteLine($"Выдано студенту {item.NameOfStudent}  {item.NumbersCoffe} чашек кофе");
			}
			else
			{
				Console.WriteLine("Кофе выдано не будет");
			}
		}

		internal static void Display(StudentCollection print)
		{
			Console.WriteLine();
			foreach (var item in print)
			{
				Console.WriteLine($"Студент {item.NameOfStudent} получил {item.NumbersCoffe} чашек кофе");

			}
		}

	}
}
