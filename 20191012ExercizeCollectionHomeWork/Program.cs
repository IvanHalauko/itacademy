﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191012ExercizeCollectionHomeWork
{
	//1) (выполнено) Создать программу выдачи кофе студентам.
	//2) (выполнено) Список студентов наполнить вручную.Для организации cписка 
	//3) (не выполнено) использовать классы Stack и (или) Queue.
	//Обратить внимание в каком порядке студенты получат 
	//кофе при использовании этих классов.

	class Program
	{
		private static string nameStudent;
		private static int numberCoffe;

		static void Main(string[] args)
		{
			string isStudent;
			Console.WriteLine("Вы студент этого ВУЗа? (Y/N)");
			isStudent = Console.ReadLine();

			StudentCollection studentCollection = new StudentCollection();

			//Создаем Queue<int> numbers = new Queue<int>();

			for (int i = 0; i < 3; i++)
			{
				if (isStudent == "y" || isStudent == "Y")
				{
					Student newStudent = CreateStudent();
					studentCollection.InsertStudent(newStudent);
					Console.WriteLine("Полный список студентов, получивших кофе:");
					StudentCollection.Display(studentCollection);

				}
				else if ((isStudent == "n" || isStudent == "N"))
				{
					Console.WriteLine("Автомат не выдает кофе НЕ студентам");
					break;
				}
				else
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine("Вы нажали недопустимую кнопку. Программа будет закрыта");
				}
			}
			Console.WriteLine("Кофе закончилось!");

		}


		private static Student CreateStudent()
		{
			//Console.WriteLine("Вы студент");
			Console.WriteLine("Введите Ваше имя:");
			nameStudent = Console.ReadLine();

			Console.WriteLine("Введите количество порций");
			int.TryParse(Console.ReadLine(), out numberCoffe);
			//Student student = new Student() {NameOfStudent=nameStudent, NumbersCoffe = numberCoffe };
			//return student;
			return null;
		}

	}


}
