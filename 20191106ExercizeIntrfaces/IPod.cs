﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191106ExercizeIntrfaces
{
	public interface IPod
	{
		string Collor { get; set; }
		IEnumerable<string> GetTracs();

		int GetSoundsTime();
	}
}
