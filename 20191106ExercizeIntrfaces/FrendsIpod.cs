﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191106ExercizeIntrfaces
{
	public class FrendsIpod : IPod
	{
		public string Collor { get; set; } = "Red";

		public int GetSoundsTime()
		{
			return 16;
		}

		public IEnumerable<string> GetTracs()
		{
			return new List<string>
			{
				"Scorpions",
				"Metallica",
				"Bonjovi",
			};
		}
	}
}
