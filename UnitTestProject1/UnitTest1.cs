﻿using System;
using _20191106ExercizeIntrfaces;

using Microsoft.VisualStudio.TestTools.UnitTesting;
//U
namespace UnitTestProject1
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void Test_GetSoundsTime_For_MyIpod()
		{
			IPod myIpod = new MyIpod();
			Assert.AreEqual(16, myIpod.GetSoundsTime());
		}
	}
}
