﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWork
{
	//1.(Выполнено) Создать коллекцию (размерность установить с консоли), заполнить коллекцию строками,
	//2. далее вывести на консоль строки длина которых больше 3 и меньше 6 символов.
	class Program
	{
		static void Main(string[] args)
		{
			int p=0;
			Console.WriteLine("Введите размерность списка");
			int.TryParse(Console.ReadLine(), out p);
			
			List<string> collectionString = new List<string>(); //создаем коллекцию
			Console.WriteLine("Введите строки списка через ENTER");
			for (int i = 0; i < p; i++)
			{
				string x = Console.ReadLine();
				collectionString.Add(x);
			}
			Console.WriteLine(" ");
			Console.WriteLine("Полный список:");
			foreach (var item in collectionString)
			{
				Console.WriteLine(item);
			}

			//2. далее вывести на консоль строки длина которых больше 3 и меньше 6 символов.
			Console.WriteLine("Список строк длины, которых больше 3 и меньше 6 символов:");
			foreach (var item in collectionString)
			{
				if (item.Length > 2 && item.Length<7)
					Console.WriteLine(item);
				else
				{
					Console.WriteLine("строки в списке отсутствуют");
				}
			}
		}
	}
}
