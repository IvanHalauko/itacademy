﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaCalculation
{		//Работа с методом и его вызов в Main
	class Program
	{
		public const double PI = 3.141;

		static void Main(string[] args)
		{
			int d = 0;

			Console.WriteLine("Area calculation");
			Console.WriteLine("Enter circle diametr D (mm.):");
			//x=Console.ReadLine
			int.TryParse(Console.ReadLine(), out d);

			Console.WriteLine(GetCircleArea(d));
			Console.ReadLine();

		}
		static double GetCircleArea(int d)
		{
			{
				Console.WriteLine($"Area circle with diametr  {d}:");
				return (PI / 4) * Math.Pow(d, 2);
			}

		}
	}
}
