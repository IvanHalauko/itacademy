﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191009ExercizeArray2D
{
	class Array3DProgram
	{
		static void Main(string[] args)
		{
			int sizeArray = 2;
			//Console.WriteLine("Введите размерность массива");

			//int.TryParse(Console.ReadLine(), out sizeArray);

			int[,,] data_1 = new int[sizeArray, sizeArray, sizeArray];
			
			for (int i = 0; i < sizeArray; i++)
			{
				int.TryParse(Console.ReadLine(), out data_1[i,i,i]);
			}

			//for (int i = 0; i < sizeArray; i++)
			//{
			//	Console.WriteLine(data_1[i]);
			//}
		}
	}
}
