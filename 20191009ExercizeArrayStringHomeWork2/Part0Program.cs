﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191009ExercizeArrayStringHomeWork2
{
	// Используя домашнее задание с предыдущего ДЗ написать следующее:
	// 1. Создать двухмерный массив любой размерностью[n, n] типа int
	// 2. Программа должна автоматически инициализировать все элементы массива значениями 
	// от 1 до 9 используя Random
	// 3. Далее в цикле нажимая клавиши WSAD производить смещение по элементам массива
	// 4. Если игрок попадает на ячейку с числом которое делится на 3 без остатка, то игрок проиграл
	// 5. Игра начинается с позиции 0:0 (в этой ячейке ни когда не числа кратного 3-ём)
	// 6. Игра считается успешно завершенной по достижению ячейки n:n(т.е.дальнего угла массива)
	// 7. По нажатию кнопки Esc игрок может завершить игру в любой момент.

	class Part0Program
	{
		static void Main(string[] args)
		{
			int sizeArr = 9;
			int[,] battleFieldArr = new int[sizeArr, sizeArr];

			Random rand = new Random(); //объявление переменной для генерации случайных чисел
			for (int i = 0; i < sizeArr; i++)
			{
				for (int j = 0; j < sizeArr; j++)
				{
					battleFieldArr[i, j] = rand.Next(1, 9);
					if (battleFieldArr[i, j] % 3 == 0)
					{
				Console.ForegroundColor = ConsoleColor.Red;
					}
					else Console.ForegroundColor = ConsoleColor.White;
						//Console.Write(battleFieldArr[i, j] + "");
				}
			}

		}
	}
}



//for (int rowIndex = 0; rowIndex < sizeArr; rowIndex++)
			//{
			//	int[] row = Enumerable.Range(0, battleFieldArr.GetUpperBound(1) + 1).
			//		Select(i => battleFieldArr[rowIndex, i]).ToArray(); //Выборка строки из двумерного массива и передача в одномерный массив
			//	//var maxValue = row.Max();       //Поиск максимального элемента в двумерном массиве
			//	Console.Write($" массив:{rowIndex} элементы массива:");
			//	foreach (var element in row)
			//	{
			//		Console.Write(element);
			//		Console.Write(" ");
			//	}
			//	Console.WriteLine("");
			//}