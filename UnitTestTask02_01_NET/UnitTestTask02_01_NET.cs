﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task02_01_NET;

namespace UnitTestTask02_01_NET
{
	[TestClass]
	public class UnitTestTask02_01_NET
	{
		[TestMethod]
		public void GivenOverrideOperatorPlus_WhenForTwoVectors_1_1_1_and_2_2_2_ThenIs_3_3_3()
		{
			Vector vectoFirst = new Vector(1, 1, 1);
			Vector vectorSecond = new Vector(2, 2, 2);
			Vector expectedVector = new Vector(3, 3, 3);

			Vector vectorActual = vectoFirst + vectorSecond;
			Assert.AreEqual(expectedVector,vectorActual);
		}

		//public void GivenEuclideanAlgorithm_WhenForTwoNumb_200_100_ThenIs100()
		//{
		//	Assert.AreEqual(100, AlgorithmGCD.EuclideanAlgorithm(200, 100, out double elapsedTime));
		//}

	}
}
