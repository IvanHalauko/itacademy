﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191012ExercizeCollectionPart2CustomCollection
{
	public class AnimalCollection : Collection<Animal>
	{
		protected override void InsertItem(int index, Animal item)
		{
			if (item != null)
			{
				base.InsertItem(index, item); //base указывает на обращение к родительскому классу
				Console.WriteLine($"You added new animal: {item.Voice}");				
			}
			else
			{
				Console.WriteLine("Animal cannot be null!");
			}
		}

		// удалить и добавить
		protected override void RemoveItem(int index)
		{
			Console.WriteLine($"Вы удалите:{Items[index].Voice}");
			base.RemoveItem(index);
		}


	}
}
