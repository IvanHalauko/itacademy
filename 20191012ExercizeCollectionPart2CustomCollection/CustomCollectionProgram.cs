﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191012ExercizeCollectionPart2CustomCollection
{
	class CustomCollectionProgram
	{
		static void Main(string[] args)
		{
			var dog = new Animal { Voice = "Gav" };
			var cat = new Animal { Voice = "Miaw" };

			var collection = new AnimalCollection();


			collection.Insert(0, dog);
			collection.Insert(1, cat);

			Console.WriteLine(collection.Count);

		}
	}
}
