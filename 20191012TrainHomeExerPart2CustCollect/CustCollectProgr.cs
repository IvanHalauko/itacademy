﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191012TExerPart3CustomCollection
{
	class CustCollectProgr
	{
		static void Main(string[] args)
		{
			var dog = new Animal {Voice="Gav",Name = "Dog"};
			var cat = new Animal { Name = "Cat", Voice = "Mau" };
			int ind;
			//как сделать: 
			//1) что новый объект создавался после ввода из консоли его полей  
			//2) потом заносились в коллекцию 
			//сейчас оно берет проинициализированный объект dog и cat и сразу выводит 
			//после нажатия на одну букву


			var anim = new Animal { }; //создал объект без параметров
			var collection = new AnimalCollection();
			int.TryParse(Console.ReadLine(), out ind);

			collection.Insert(ind, anim); //добавляю в коллекцию объект без полей

			collection.Insert(ind, dog);
			collection.Insert(ind, cat);



			//Queue<Animal> collection = new Queue<Animal>();

			//collection.Enqueue(new Animal());



			////Блок A вопрос 1: что нужно что б вывести в Main?
			//collection.Display();


			//Блок A вопрос 1: вот так работает в Main 
			//Выводит всю коллекцию
			Console.WriteLine("полный список всех животных:");
			Display(collection);
			void Display(Collection<Animal> print)
			{
				Console.WriteLine();
				foreach (var item in print)
				{
					Console.WriteLine($"{item.Name}");
				}
			}
		}
	}
}
