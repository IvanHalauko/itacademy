﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace _20191012TExerPart3CustomCollection
{
	class AnimalCollection : Collection<Animal>
	//class AnimalCollection : Collection<Animal>
	{
		protected override void InsertItem(int index, Animal item)

		{
			if (item != null)
			{
				base.InsertItem(index, item);
				Console.WriteLine($"Вы добавили новое животное:{item.Name} {item.Voice}");			
			}
			else
			{
				Console.WriteLine("Вы ничего не добавили");
			}
		}

		//Удалить животное
		protected override void RemoveItem(int index)
		{
			Console.WriteLine($"Вы удалили {Items[index].Voice}");
			base.RemoveItem(index);
		}



		////Блок A вопрос 1: что нужно что б вывести в Main?
		//public void Display(Collection<Animal> print)
		//{
		//	Console.WriteLine();
		//	foreach (var item in print)
		//	{
		//		Console.WriteLine($"{item.Name}");
		//	}
		//}

		
	}
}
