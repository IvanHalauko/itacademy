﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			//var tmp = new Timer(TimerTic, null, 10, 2000); // каждые 2 секунды вызвает событие

			//Console.ReadLine();
			//tmp = null;

			Console.WriteLine(GC.GetTotalMemory(false));
			for (int i = 0; i < 1000000; i++)
			{
				var obj = (object)i;
			}
			Console.WriteLine(GC.GetTotalMemory(false));

			GC.Collect();

			Console.WriteLine(GC.GetTotalMemory(false));

		}

		private static void TimerTic(object state)
		{
			Console.WriteLine("Doing something.");
			GC.Collect();
			Console.WriteLine("Invoke GC");
		}

		


	}
}
