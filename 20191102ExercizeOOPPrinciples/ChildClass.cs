﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191102ExercizeOOPPrinciples
{
	public class ChildClass : BaseClass // потому что запечатан базовый класс
	{
		public ChildClass()
		{
			Name = "qweeqwe"; 
			
		}
		public int MyProperty { get; set; }


		public sealed override string GetName()
		{
			return $"{Name}";
		}
		public string GetIncorrectName()
		{
			return "dfdf";
		}


	}
}
