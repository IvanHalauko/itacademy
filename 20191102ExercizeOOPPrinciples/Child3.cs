﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191102ExercizeOOPPrinciples
{
	public class Child3 : BaseClass3
	{
		private readonly int id;

		public string LastName => $"FirstName {base.Name }";

		public Child3() : base ("just name")
		{
			
		}
		public Child3(string name, int id):base(name)
		{
			this.id = id;
		}

	}
}
