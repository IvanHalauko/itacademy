﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeCredit
{
	class Program
	{
		static void Main(string[] args)
		{
			double credit = 0; //сумма, полученная в кредит
			double percent = 0; // годовая процентная ставка
			int month = 0; // кол-во месяцев 
			double payMainDebt = 0; //ежемесячный платеж по основному долгу
			double payInMonth = 0; // величина ежемесячного платежа
			double monthPercent = 0; // коэффициент ежемесячного платежа,
			double totalРayout = 0; //
			//int numberOfPayment = 0;
			
			Console.WriteLine("Enter credit:");
			//int.TryParse(Console.ReadLine(), out x);
			double.TryParse(Console.ReadLine(), out credit);
			Console.WriteLine("Enter percent:");
			double.TryParse(Console.ReadLine(), out percent);
			Console.WriteLine("Enter number of months:");
			int.TryParse(Console.ReadLine(), out month);

			monthPercent = (percent / 100) / 12; // коэффициент ежемесячного платежа, т.к. процентная ставка всегда годовая
			payMainDebt = credit / month;  //ежемесячный платеж по основному долгу


			payInMonth = payMainDebt + payMainDebt * monthPercent; // величина ежемесячного платежа + процент за пользование кредитом
			totalРayout = payInMonth * month;

			for ( int i=0; i<month; i++)
				{
				Console.WriteLine($"{i} платеж: {payInMonth}. Платеж по основному долгу: {payMainDebt}. Сумма за пользование кредитом {payMainDebt * monthPercent}");
				}
			Console.WriteLine($"Общая сумма платежа {totalРayout}");
		}

	}
}
