﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task02_02_NET;

namespace UnitTestTask02_02_NET
{
	[TestClass]
	public class UnitTest1
	{
		/// <summary>
		/// Test for correct calculation the mathimatical "plus" for working with two 
		/// polynoms after overloading operator. 
		/// </summary>
		[TestMethod]
		public void GivenOverrideOperatorPlus_WhenForTwoPolynomials_1_2_3_and_1_1_1_ThenIs_2_3_4()
		{
			double[] сoefPolynomFirst = new double[3] { 1, 2, 3 };
			Polynomial polynomialFirst = new Polynomial(сoefPolynomFirst);
			double[] сoefPolynomSecond = new double[3] { 1, 1, 1 };
			Polynomial polynomialSecond = new Polynomial(сoefPolynomSecond);
			double[] сoefExpectedPolynom = new double[3] { 2, 3, 4 };
			Polynomial expectedPolynom = new Polynomial(сoefExpectedPolynom);
			Polynomial actualPolynom = polynomialFirst + polynomialSecond;
			CollectionAssert.AreEqual(expectedPolynom.СoefPolynom, actualPolynom.СoefPolynom);
		}
		/// <summary>
		/// Test for correct calculation the mathimatical "minus" for working with two 
		/// polynoms after overloading operator. 
		/// </summary>
		[TestMethod]
		public void GivenOverrideOperatorMinus_WhenForTwoPolynomials_5_4_3_2_and_1_1_1_1_ThenIs_4_3_2_1()
		{
			double[] coefPolinomFirst = new double[4] { 5, 4, 3, 2 };
			Polynomial polynomialFirst = new Polynomial(coefPolinomFirst);
			double[] coefPolinomSecond = new double[4] { 1, 1, 1, 1 }; 
			Polynomial polynomialSecond = new Polynomial(coefPolinomSecond);
			double[] coefExpectedPolynom = new double[4] { 4, 3, 2, 1 };
			Polynomial expectedPolynom = new Polynomial(coefExpectedPolynom);
			Polynomial actualPolynom = polynomialFirst - polynomialSecond;
			CollectionAssert.AreEqual(expectedPolynom.СoefPolynom, actualPolynom.СoefPolynom);
		}
		/// <summary>
		/// Test for correct calculation the mathimatical "muliply" for working with two 
		/// polynoms after overloading operator. 
		/// </summary>
		[TestMethod]
		public void GivenOverrideOperatorMultiply_WhenForTwoPolynomials_5_4_3_2_and_1_1_1_1_ThenIs_4_3_2_1()
		{
			double[] coefPolinomFirst = new double[2] { 5, 4 };
			Polynomial polynomialFirst = new Polynomial(coefPolinomFirst);
			double[] coefPolinomSecond = new double[2] { 1, 1};
			Polynomial polynomialSecond = new Polynomial(coefPolinomSecond);
			double[] coefExpectedPolynom = new double[3] { 5, 9, 4 };
			Polynomial expectedPolynom = new Polynomial(coefExpectedPolynom);
			Polynomial actualPolynom = polynomialFirst * polynomialSecond;
			CollectionAssert.AreEqual(expectedPolynom.СoefPolynom, actualPolynom.СoefPolynom);
		}




	}
}
