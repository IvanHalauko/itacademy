﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191020ExercizeClassPart1HomeWork
{
	class Student
	{
		//Поля: имя, фамилия, оценки(коллекция  int)
		public string Name { get; set; }
		public string Surname { get; set; }
		public List<int> Marks { get; set; }

		public Student (string name, string surname, int marks)
		{
			Name = name;
			Surname = surname;
			Marks = new List<int>();

		}


	}
}
