﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace task01_NET
{
	public class AlgorithmGCD
	{
		/// <summary>
		/// The Euclidean algorithm calculates (GCD) of two natural numbers numOne and numTwo.
		/// </summary>
		/// <param name="numOne">number one</param>
		/// <param name="numTwo">number two</param>
		/// <returns>GCD of two numbers</returns>
		public static int EuclideanAlgorithm(int numOne, int numTwo, out double elapsedTime)
		{
			//elapsedTime;
			var watch = new Stopwatch();
			watch.Start();
			//Thread.Sleep(1000);
			
			while (numOne != numTwo)
			{
				if (numOne > numTwo)
				{
					numOne = numOne - numTwo;
				}
				else
				{
					numTwo = numOne - numTwo;
				}
			}
			watch.Stop();
			elapsedTime = watch.ElapsedMilliseconds;
			return numOne;
		}
		/// <summary>
		/// The method calculates (GCD) of three natural numbers numOne, numTwo, numThree.
		/// </summary>
		/// <param name="numOne">number one</param>
		/// <param name="numTwo">number two</param>
		/// <param name="numThree">number three</param>
		/// <returns>GCD of three numbers</returns>
		public static int EuclideanAlgorithm(int numOne, int numTwo, int numThree, out double elapsedTime)
		{
			//elapsedTime = 0;
			var watch = new Stopwatch();
			watch.Start();
			//Thread.Sleep(3000);
			int tempNumb = EuclideanAlgorithm(numOne, numTwo, out elapsedTime);
			int gcdThreeNumb = EuclideanAlgorithm(tempNumb, numThree, out elapsedTime);
			watch.Stop();
			elapsedTime = watch.ElapsedMilliseconds;
			return gcdThreeNumb;

		}

		/// <summary>
		/// The method calculates (GCD) of four natural numbers numOne, numTwo, numThree and numFour.
		/// </summary>
		/// <param name="numOne">number one</param>
		/// <param name="numTwo">number two</param>
		/// <param name="numThree">number three</param>
		/// <param name="numFour">number four</param>
		/// <returns>GCD of four numbers</returns>
		public static int EuclideanAlgorithm(int numOne, int numTwo, int numThree, int numFour, out double elapsedTime)
		{
			//elapsedTime = 0;
			var watch = new Stopwatch();
			watch.Start();
			//Thread.Sleep(4000);	
			int tempNumb = EuclideanAlgorithm(numOne, numTwo, out elapsedTime);
			int gcdThreeNumb = EuclideanAlgorithm(tempNumb, numThree, out elapsedTime);
			int gcdFourNumb = EuclideanAlgorithm(gcdThreeNumb, numFour, out elapsedTime);
			watch.Stop();
			elapsedTime = watch.ElapsedMilliseconds;
			return gcdFourNumb;
		}


		/// <summary>
		/// The method calculates (GCD) of two natural numbers numOne, numTwo.
		/// </summary>
		/// <param name="numOne">number one</param>
		/// <param name="numTwo">number two</param>
		/// <param name="elapsedTime">elapsedtime</param>
		/// <returns>GCD of four numbers and elapsedtime</returns>
		public static int SteinsAlgorithm(int numOne, int numTwo, out double elapsedTime)
		{
			//elapsedTime = 0;
			var watch = new Stopwatch();
			watch.Start();		
			int gcd = 1;
			int tmp;
			if (numOne == 0)
			{
				watch.Stop();
				elapsedTime = watch.ElapsedMilliseconds;
				return numTwo;
			}
			if (numTwo == 0)
			{
				watch.Stop();
				elapsedTime = watch.ElapsedMilliseconds;
				return numOne;
			}
			if (numOne == numTwo)
			{
				watch.Stop();
				elapsedTime = watch.ElapsedMilliseconds;
				return numOne;
			}
			if (numOne == 1 || numTwo == 1)
			{
				watch.Stop();
				elapsedTime = watch.ElapsedMilliseconds;
				return 1;
			}
			while (numOne != 0 && numTwo != 0)
			{

				if (numOne % 2 == 0 && numTwo % 2 == 0)
				{
					gcd *= 2;
					numOne /= 2;
					numTwo /= 2;
					continue;
				}
				if (numOne % 2 == 0 && numTwo % 2 != 0)
				{
					numOne /= 2;
					continue;
				}
				if (numOne % 2 != 0 && numTwo % 2 == 0)
				{
					numTwo /= 2;
					continue;
				}
				if (numOne > numTwo)
				{
					tmp = numOne;
					numOne = numTwo;
					numTwo = tmp;
				}
				tmp = numOne;
				numOne = (numTwo - numOne) / 2;
				numTwo = tmp;
			}
			if (numOne == 0)
			{
				watch.Stop();
				elapsedTime = watch.ElapsedMilliseconds;
				return gcd * numTwo;
			}
			else
			{
				watch.Stop();
				elapsedTime = watch.ElapsedMilliseconds;
				return numOne;
			}
		}

		/// <summary>
		/// The method calculates (GCD) of four natural numbers numOne, numTwo, numThree and numFour.
		/// </summary>
		/// <param name="numOne">number one</param>
		/// <param name="numTwo">number two</param>
		/// <param name="numThree">number three</param>
		/// <param name="elapsedTime">elapsed time</param>
		/// <returns>GCD of four numbers</returns>
		public static int SteinsAlgorithm(int numOne, int numTwo, int numThree, out double elapsedTime)
		{
			//elapsedTime = 0;
			var watch = new Stopwatch();
			watch.Start();
			//Thread.Sleep(3000);
			int tempNumb = SteinsAlgorithm(numOne, numTwo, out elapsedTime);
			int gcdThreeNumb = SteinsAlgorithm(tempNumb, numThree, out elapsedTime);
			watch.Stop();
			elapsedTime = watch.ElapsedMilliseconds;
			return gcdThreeNumb;
		}

		/// <summary>
		/// The method calculates (GCD) of four natural numbers numOne, numTwo, numThree and numFour.
		/// </summary>
		/// <param name="numOne">number one</param>
		/// <param name="numTwo">number two</param>
		/// <param name="numThree">number three</param>
		/// <param name="numFour">number four</param>
		/// <param name="elapsedTime">elapsed time</param>
		/// <returns>GCD of four numbers</returns>
		public static int SteinsAlgorithm(int numOne, int numTwo, int numThree, int numFour, out double elapsedTime)
		{
			//elapsedTime = 0;
			var watch = new Stopwatch();
			watch.Start();
			//Thread.Sleep(4000);	
			int tempNumb = SteinsAlgorithm(numOne, numTwo, out elapsedTime);
			int gcdThreeNumb = SteinsAlgorithm(tempNumb, numThree, out elapsedTime);
			int gcdFourNumb = SteinsAlgorithm(gcdThreeNumb, numFour, out elapsedTime);
			watch.Stop();
			elapsedTime = watch.ElapsedMilliseconds;
			return gcdFourNumb;
		}

		/// <summary>
		/// The method prepare data for graphical analysis
		/// </summary>
		/// <returns>prepare data for histogramm</returns>
		public static List<DataForTheGraph> PrepareDataForTheGraph()
		{
			double overallTimeSteinsAlgorithm;
			double overallTimeEuclideanAlgorithm;

			List<DataForTheGraph> dataForTheGraphs = new List<DataForTheGraph>();
			var testEuclideanForTwo = EuclideanAlgorithm(200, 100, out overallTimeEuclideanAlgorithm);
			var testSteinForTwo = SteinsAlgorithm(200, 100, out overallTimeSteinsAlgorithm);
			dataForTheGraphs.Add(new DataForTheGraph("2",overallTimeSteinsAlgorithm, overallTimeEuclideanAlgorithm));

			var testEuclideanForThree = EuclideanAlgorithm(200, 100,50, out overallTimeEuclideanAlgorithm);
			var testSteinForThree = SteinsAlgorithm(200, 100,50, out overallTimeSteinsAlgorithm);
			dataForTheGraphs.Add(new DataForTheGraph("3", overallTimeSteinsAlgorithm, overallTimeEuclideanAlgorithm));

			var testEuclideanForFour = EuclideanAlgorithm(200, 100,50,10, out overallTimeEuclideanAlgorithm);
			var testSteinForFour = SteinsAlgorithm(200, 100,50,10, out overallTimeSteinsAlgorithm);
			dataForTheGraphs.Add(new DataForTheGraph("4", overallTimeSteinsAlgorithm, overallTimeEuclideanAlgorithm));

			return dataForTheGraphs;
		}
	}
}
