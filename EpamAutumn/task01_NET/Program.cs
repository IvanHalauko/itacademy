﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


//1) (выполнено+тест) Разработать тип, реализующий алгоритм Евклида для вычисления НОД двух целых чисел 
//(http://en.wikipedia.org/wiki/Euclidean_algorithm).  
//2) (выполнено+тест) Добавить к разработанному типу дополнительную функциональность в виде 
//перегруженных методов вычисления НОД для трех, четырех или пяти целых чисел.  
//3) (выполнено+тест) Добавить к разработанному типу метод, реализующий алгоритм Стейна 
//(бинарный алгоритм Эвклида) для расчета НОД двух целых чисел 
//(http://en.wikipedia.org/wiki/Binary_GCD_algorithm). Метод должен 
//принимать выходной параметр, содержащий значение времени, затраченное для выполнения расчетов. 
//4) (выполнено+тест) Изменить метод, реализующий алгоритм Евклида вычисления НОД двух параметров, добавив 
//аналогичную функциональность. 
//5) (выполнено+тест) Дополнить методом, подготавливающим данные для построения гистограммы, сравнивающей время 
//нахождения решения каждым из методов. 
//6) (выполнено+тест) Создать unit-тесты для тестирования разработанных методов. 
//7) (выполнено)Весь код должен быть снабжён комментариями (автокомментариями) 
namespace task01_NET
{
	class Program
	{
		static void Main(string[] args)
		{
			//алгоритм Евклида для вычисления НОД двух целых чисел
			double elapsedTime = 0;

			AlgorithmGCD testOne = new AlgorithmGCD();
			//Console.WriteLine(testOne.EuclideanAlgorithm(200, 100, out elapsedTime));
			//Console.WriteLine(testOne.EuclideanAlgorithm(200, 100,20, out elapsedTime));
			Console.WriteLine(AlgorithmGCD.EuclideanAlgorithm(200, 100, 10, 2, out elapsedTime));
			
			//Console.WriteLine(testOne.SteinsAlgorithm(15,5, out elapsedTime));
			Console.WriteLine(elapsedTime);

		}

	}
}
