﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191113ExercizeDelegat
{
	class Program
	{
		delegate void MyDelegate(string str);

		static void Main(string[] args)
		{
			var parentDelegate = new MyDelegate(TestMyDelegate);
			///!!!
			//var parentDelegate2 = new MyDelegate(TestMyDelegate2);
			//var newDelegate=(MyDelegate)Delegate.Combine(parentDelegate, parentDelegate2);
			//newDelegate("New string");
			///!!!!

			parentDelegate += TestMyDelegate2;
			//parentDelegate += programm.TestMyDelegateNonStatic;
			parentDelegate("Just string");



		}

		static void TestMyDelegate(string str)
		{
			Console.WriteLine($"Static TestMyDelegate{(string.IsNullOrEmpty(str)? "String is Empty":str)}");
		}


		void TestMyDelegateStatic(string str)
		{
			Console.WriteLine($"Static TestMyDelegateNONStatic{(string.IsNullOrEmpty(str) ? "String is Empty" : str)}");
		}

		static void TestMyDelegate2(string str)
		{
			Console.WriteLine($"Static TestMyDelegate2{(string.IsNullOrEmpty(str) ? "String is Empty" : str)}");
		}


	}



}
