﻿//using Calculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191005ExerciseGameShowCoordinate
{
	//HomeTask after 05.10.2019 exercise 
	//Ввести размеры поля(целочисленные X и Y). Далее в цикле проверять какая клавиша клавиатуры была 
	//нажата.Если была нажата клавиша ESC, то завершить выполнение программы, если были 
	//нажаты(W, S, A, D), то выводить на экран на какую координату(точку) мы 
	//переместились(вверх, вниз, влево, вправо соответственно) !!!ИСПОЛЬЗОВАТЬ Switch и 
	//любой цикл!!!. При попытке выхода за границы поля, выводить сообщение.Шаг хода всегда 1.


	class GameProgram
	{
		static void Main(string[] args)
		{

			
			GameProgram b = new GameProgram();
			Console.WriteLine(b);
			int sizeX = 0;
			int sizeY = 0;
			Console.WriteLine("Enter size X of area: ");
			//int.TryParse(Console.ReadLine(), out x);
			int.TryParse(Console.ReadLine(), out sizeX);
			Console.WriteLine("Enter size Y of area: ");
			int.TryParse(Console.ReadLine(), out sizeY);
			Console.WriteLine($"size of area:{sizeX} ; {sizeY} ");

			int x = 0;
			int y = 0;
			

			Console.WriteLine("Вы хотите играть? Y/N");
			ConsoleKeyInfo button = Console.ReadKey();
			Console.WriteLine();
			while (button.Key == ConsoleKey.Y)
			{

				while (x<=sizeX && y<=sizeY) //  && (x<-1) && (y<-1))
				{
					Console.WriteLine(" ");
					Console.WriteLine("Нажмите A/S/D/W/ESC");
					ConsoleKeyInfo directionOfTravel = Console.ReadKey();
					//Console.WriteLine("Нажмите ENTER");

					switch (directionOfTravel.Key)
					{
						case ConsoleKey.W: //W - up Y++
							
							y = ChangeCoordinate(x, y, directionOfTravel);
							Console.WriteLine("");
							Console.WriteLine($"Координата x:{x}, Координата y:{y}");
							//Console.WriteLine("Нажмите ENTER");
							break;

						case ConsoleKey.S: //S - down Y--
							y =ChangeCoordinate(x, y, directionOfTravel);
							Console.WriteLine("");
							Console.WriteLine($"Координата x:{x}, Координата y:{y}");
							//Console.WriteLine("Нажмите ENTER");
							break;

						case ConsoleKey.D: // D - right X++

							x = ChangeCoordinate(x, y, directionOfTravel);
							Console.WriteLine("");
							Console.WriteLine($"Координата x:{x}, Координата y:{y}");
							//Console.WriteLine("Нажмите ENTER");
							break;

						case ConsoleKey.A: // A -Left X--
							x =  ChangeCoordinate(x: x, y: y, consoleKey: directionOfTravel);
							Console.WriteLine("");
							Console.WriteLine($"Координата x:{x}, Координата y:{y}");
							//Console.WriteLine("Нажмите ENTER");
							break;
						case ConsoleKey.Escape: // Преостанавливаем работу метода по нажатию на ESC
							return;
						default:
							Console.WriteLine("Вы нажали на недопустимую кнопку");
							break;
					}

					//Console.WriteLine($"Result:{x + ChangeCoordinate(x, y, directionOfTravel)}");
					//Console.ReadLine();
				}

				Console.WriteLine("Вы проиграли! Вы вышли за поле игры");
				Console.WriteLine("Вы хотите продолжить игру? Esc/Y");
				Console.WriteLine(" ");
				ConsoleKeyInfo button1 = Console.ReadKey();
				if (button1.Key == ConsoleKey.Escape)
				{
					Console.WriteLine("Игра окончена");
					break;
				}
				//else if (button1.Key == ConsoleKey.Y)
				//{
				//	ChangeCoordinate(x, y, directionOfTravel);
				//}

			}
		}

		static int ChangeCoordinate(int x, int y, ConsoleKeyInfo consoleKey)
		{
			switch (consoleKey.Key)
			{
				case ConsoleKey.W: //W - up Y++
					return ++y;

				case ConsoleKey.S: //S - down Y--
					return --y;

				case ConsoleKey.D: // D - right X++
					return ++x;

				case ConsoleKey.A: // A -Left X--
					return --x;
				case ConsoleKey.Escape: // Преостанавливаем работу метода по нажатию на ESC
					break;
				default:
					Console.WriteLine("Вы нажали на недопустимую кнопку");
					return 100;
			}
			Console.WriteLine("Вы нажали на недопустимую кнопку");
			return 30;
		}


	}
}
