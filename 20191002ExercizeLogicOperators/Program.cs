﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191002Exercize
{// Работа с логическими операторами
	class Program
	{
		static void Main(string[] args)
		{
			short x = 8;
			short y = 5;
			
			var d = (x == y);
			Console.WriteLine($"Результат операции =={d}");

			var e = (x != y);
			Console.WriteLine($"Результат операции !={e}");

			var f = (x > y);
			Console.WriteLine($"Результат операции >{f}");


			var g = (x < y);
			Console.WriteLine($"Результат операции <{g}");


			var k = (x >= y);
			Console.WriteLine($"Результат операции >={k}");

			var m = (x <= y);
			Console.WriteLine($"Результат операции <={m}");

		}
	}
}
