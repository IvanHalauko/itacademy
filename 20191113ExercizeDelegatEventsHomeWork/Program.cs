﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191113ExercizeDelegatEventsHomeWork
{

	//1) (Выполнено) Разработать класс НовостнойПортал, в котором есть список категорий 
	//новостей (погода, юмор, спорт).
	//2) (Выполнено) Разработать класс ПодписчикПортала.
	//Программа реализует следующий алгоритм:
	//3) (Выполнено) Я как пользователь выбираю категорию с клавиатуры, 
	//4) (Выполнено) вижу два пункта на выбор(опубликовать новость, добавить подписчика в текущую категорию)
	//5) (выполнено) Если я выбираю первый пункт, то после ввода текста и нажатия на Enter, все 
	//подписчики категории получают оповещения(выводятся на экран)
	//6) (Выполнено)Если я добавляю нового подписчика, то нужно ввести его имя(оно будет 
	//выводится когда он получает оповещение о новости)

	
	public delegate void ChoiceTypeNews();
	public delegate NewsCollection ChoiceAction(NewsCollection collectionNews, string button);
	public delegate SubscribersCollection ChoiceActionTwo(SubscribersCollection collectionSubscribers,string button);

	public class Keyboard
	{
		public event ChoiceTypeNews PressKeyH = null; //Create event on button H
		public event ChoiceAction PressKeyC = null; //Create and publish news
		public event ChoiceActionTwo PressKeyA = null; //Create event on button Add Subscriber

		private NewsCollection portalNews = new NewsCollection();
		private SubscribersCollection portalSubscribers = new SubscribersCollection();

		public string button;
		public string Button { get; set; }

		public void PressKeyHEvent() //Вызов события, связанного с клавишей H
		{
			if (PressKeyH != null) 
			{
					PressKeyH.Invoke();//Invoke Event
			}
		}

		public void PressKeyCEvent(string button) //create news
		{
			if (PressKeyC != null)
			{
				PressKeyC.Invoke(portalNews,button); //Invoke Event
			}

		}
		public void PressKeyAEvent(string button) //create subscriber
		{
			if (PressKeyA !=null) 
			{
				PressKeyA.Invoke(portalSubscribers,button);  //Invoke Event
			}
		}

		public void StartProgramm(out string buttonOut)
		{
			buttonOut="";
			
			while (true)
			{
				Console.WriteLine("Push first letter of new's category:");
				Console.WriteLine("H-Humor, W-Weather, S-Sport, E-Leave action");
				Console.WriteLine("Make your choice and push putton:");
				Console.WriteLine("");

				buttonOut = Console.ReadLine();
				switch (buttonOut)
				{
					case "h":
					case "H":
						//Button = buttonOut;
						//PressKeyHEvent();
						GetChoiceAction(buttonOut); //метод выбора действия опубликовать новость или добавить подписчика
						break;
					case "w":
					case "W":
						//PressKeyHEvent();
						GetChoiceAction(buttonOut); //метод выбора действия опубликовать новость или добавить подписчика
						break;
					case "s":
					case "S":
						//PressKeyHEvent();
						GetChoiceAction(buttonOut); //метод выбора действия опубликовать новость или добавить подписчика
						break;
					case "e":
					case "E":
						goto Exit;
					default:
						Console.WriteLine("You chose impossible button!!!");
						break;
				}
			}
		Exit: Console.WriteLine("You left program!!");
		}

		public void GetChoiceAction(string buttonIn)
		{
			Button=buttonIn;
			while (true)		
			{
				Console.WriteLine("");
				Console.WriteLine("Push first letter of action:");
				Console.WriteLine("C-Create news, A-Add new Subscriber, E-Leave action");
				Console.WriteLine("Make your choice and push button:");
				Console.WriteLine("");
				string button = Console.ReadLine();
				switch (button)
				{
					case "C":
					case "c":
						{						
							PressKeyCEvent(buttonIn);
							ShowCollectionSubscribers(buttonIn);
							break;
						}
					case "A":
					case "a":
						{
							PressKeyAEvent(buttonIn);
							break;
						}
					case "e":
					case "E":
						goto Exit;
					default:
						Console.WriteLine("You chose impossible button!!!");
						break;
				}
			}
		Exit: Console.WriteLine("You left action!!");
		}

		public void ShowCollectionSubscribers(string buttonIn)
		{
			if (buttonIn=="h")
			{
				foreach (var item in portalSubscribers)
				{
					if (item.TypeNews==TypeNews.Humor)
					{
						Console.WriteLine(item);
					}				
				}
			}
			if (buttonIn == "w")
			{
				foreach (var item in portalSubscribers)
				{
					if (item.TypeNews == TypeNews.Weather)
					{
						Console.WriteLine(item);
					}
				}
			}
			if (buttonIn == "s")
			{
				foreach (var item in portalSubscribers)
				{
					if (item.TypeNews == TypeNews.Sport)
					{
						Console.WriteLine(item);
					}
				}
			}
		}
	}
	class Program
	{	
		//метод обработчик события H
		static private void PressKeyH_Handler()
		{
			Console.WriteLine("H");
		}	
		//метод обработчик события кнопки "C". Добавление новости в список и вывод всех 
		//подписчиков выбранной ранее категории
		static private NewsCollection PressKeyC_Handler(NewsCollection colectionNews, string button)
		{		
			Console.WriteLine("C");
			Console.WriteLine("Enter text of news:");
			string textNews = Console.ReadLine();
			if (button == "H" || button == "h")
			{
				PortalNews news = new PortalNews(TypeNews.Humor, textNews);
				colectionNews.Add(news);
			}
			if (button == "W" || button == "w")
			{
				PortalNews news = new PortalNews(TypeNews.Weather, textNews);
				colectionNews.Add(news);
			}
			if (button == "s" || button == "s")
			{
				PortalNews news = new PortalNews(TypeNews.Sport, textNews);
				colectionNews.Add(news);
			}

			Console.WriteLine("");
			//foreach (var item in colectionNews)
			//{
			//	Console.WriteLine(item);
			//}
			return colectionNews;
		}
		//метод обработчик события кнопки "A". Добавление новости в список и вывод всех 
		//подписчиков выбранной ранее категории
		static private SubscribersCollection PressKeyA_Handler (SubscribersCollection colectionSubscribers, string button)
		{									
			Console.WriteLine("A");
			Console.WriteLine("Enter new subscriber NAME:");
			string textName = Console.ReadLine();

			if (button == "H" || button == "h")
			{
				PortalSubscriber subscriber = new PortalSubscriber(TypeNews.Humor, textName);
				colectionSubscribers.Add(subscriber);
			}
			if (button == "W" || button == "w")
			{
				PortalSubscriber subscriber = new PortalSubscriber(TypeNews.Weather, textName);
				colectionSubscribers.Add(subscriber);
			}
			if (button == "S" || button == "s")
			{
				PortalSubscriber subscriber = new PortalSubscriber(TypeNews.Sport, textName);
				colectionSubscribers.Add(subscriber);
			}
			Console.WriteLine("");
			//foreach (var item in colectionSubscribers)
			//{
			//	Console.WriteLine(item);
			//}
			return colectionSubscribers;			
		}

		static void Main()
		{
			//экземпляр класса
			Keyboard keyboard = new Keyboard();
			//подписка событий на методы (делегаты с методами)
			keyboard.PressKeyH += new ChoiceTypeNews(PressKeyH_Handler);
			keyboard.PressKeyC += new ChoiceAction(PressKeyC_Handler);
			keyboard.PressKeyA += new ChoiceActionTwo(PressKeyA_Handler);

			//запуск метода, который будет следить за нажатием клавиш
			
			keyboard.StartProgramm(out string buttonOut);
		}
	}
}
