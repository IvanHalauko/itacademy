﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191113ExercizeDelegatEventsHomeWork
{
	public class PortalSubscriber
	{
		public string NameSubscriber { get; set; }
		public TypeNews TypeNews { get; set; }

		public PortalSubscriber(string nameSubscriber)
		{
			NameSubscriber = nameSubscriber;
		}
		public PortalSubscriber(TypeNews typeNews,string nameSubscriber)
		{
			NameSubscriber = nameSubscriber;
			TypeNews = typeNews;
		}
		public override string ToString()
		{
			return string.Format($"Подписчик {NameSubscriber} подписан на новости из: {TypeNews}");
		}

	}
}
