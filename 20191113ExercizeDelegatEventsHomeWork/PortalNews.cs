﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191113ExercizeDelegatEventsHomeWork
{
	public class PortalNews
	{
		public string TextNews { get; set; }
		public TypeNews TypeNews { get; set; }

		public PortalNews(TypeNews typeNews)
		{
			TypeNews = typeNews;
		}
		public PortalNews(TypeNews typeNews, string textNews)
		{
			TypeNews = typeNews;
			TextNews = textNews;
		}
		public override string ToString()
		{
			return string.Format($"Новостной раздел: {TypeNews}. Новость: {TextNews}");
		} 
	}
}
