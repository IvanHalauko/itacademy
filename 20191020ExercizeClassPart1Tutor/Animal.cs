﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191020ExercizeClassPart1
{
	public abstract class Animal //не позволяет создать экземпляр, создается для 
								 //недопускания дублирования кода 
	{
		public string Name { get; set; }
		public int Age { get; set; }
		public string Voice { get; set; }

		//Создаем конструктор, он должен только 
		//проинициализировать, никаких методов быть не должно
		//код конструктора не должен выбрасывать исключений 
		public Animal()
		{
			Name = "Noname";
			Age = 0;
			Voice = "NoVoice";
		}


		public Animal(string name)
		{
			Name = name;
		}

		public Animal(string name, int age)
		{
			Name = name;
			Age = age;
		}

		public Animal(string name, int age, string voice)
		{
			Name = name;
			Age = age;
			Voice = voice;

		}

		//Создадим два метода абстрактный и виртуальный
		public abstract string GetInformation(); //Метод не может иметь тело

		public virtual void GetVoice() // Этод метод может (Virtual) быть потенциально изменненным
		{
			Console.WriteLine(Voice); 
		}

	}
}
