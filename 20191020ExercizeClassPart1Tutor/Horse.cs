﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191020ExercizeClassPart1
{
	public class Horse:Animal
	{
		// конструктор без параметров, но вызываться будет name и age из базового класса

		public Horse()
			: base("IGoGOSha", 120) // т.к. нет параметр
		{ }

			///переопределяем метод
		public override string GetInformation()
		{
			return $"This is a Horse {Name}, age {Age};";
		}

		public override void GetVoice()
		{
			Console.WriteLine($"This Horse says iGo-GO");
		}

	}
}
