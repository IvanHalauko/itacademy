﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191020ExercizeClassPart1
{

	//добавляем класс
	public partial class Dog : Animal

	{
		public Dog(string name, int age)
			: base(name, age, "Gau")
		{ }
		

		public override string GetInformation()
		{
			return $"This is a dog{Name}, age {Age};";
		}

		partial void PartialMethod();


		
	}
}
