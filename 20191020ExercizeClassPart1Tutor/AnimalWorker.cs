﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191020ExercizeClassPart1
{
	public class AnimalWorker
	{
		private readonly List<Animal> _animals; //readonly - можем проинициализировать 
												//только в конструкторе
		public AnimalWorker()
		{
			_animals = new List<Animal>();
			GenerateAnimalsList();
		}

		public void GenerateAnimalsList()
		{
			_animals.Add(new Cat());
			_animals.Add(new Dog("Jack", 2));
			_animals.Add(new Horse());// создаем новый объект ЛОшадь

		}

		public void ShowAdditionalInformation()
		{
			foreach (var item in _animals)
			{
				Console.WriteLine(item.GetInformation()); //Отображаем весь список
			}
		}

		public void ShowVoice()
		{
			foreach (var item in _animals)
			{
				item.GetVoice();
			}
		}


	}
}
