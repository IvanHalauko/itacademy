﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191020ExercizeClassPart1
{
	class Program
	{
		static void Main(string[] args)
		{
			var worker = new AnimalWorker();
			worker.ShowAdditionalInformation();
			worker.ShowVoice();
		}
	}
}
