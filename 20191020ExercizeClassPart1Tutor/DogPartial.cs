﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191020ExercizeClassPart1
{
	public partial class Dog // при создании части (partial) класса имя части класса должно быть таким 
							//же как и его родитель, но имя файла может быть разным
	{
		public override void GetVoice()
		{
			Console.WriteLine($"This dog says {Voice}");
		}
		partial void PartialMethod()
		{
			//метод
		}

	}
}
