﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191120ExercizeTaskTryCatch
{
	class Program
	{
		static void Main(string[] args)
		{
			int x = 0;

			Console.WriteLine("Enter value from 1 until 3:");
			int.TryParse(Console.ReadLine(), out x);

			try
			{
				Console.WriteLine($"You ENTERed {x}");
			}
			catch (DivideByZeroException ex)
			{
				Console.WriteLine($"Возникло исключение {ex.Message}");

			}
		}
	}
}
