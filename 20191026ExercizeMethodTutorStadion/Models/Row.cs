﻿using System;
using System.Collections.Generic;

namespace Stadion.Models
{
    public class Row
    {
        public Row(Random random)
        {
            // Инициализируем свойство Places (т.к. оно по умолчанию Null)
            Places = new List<Place>();
            Number = random.Next();

            for (int i = 5; i > 0; i--)
            {
                // Заполняем коллекцию
                Places.Add(new Place(i, random.Next(50, 100)));
            }
        }

        public List<Place> Places { get; set; }

        public int Number { get; set; }

        public void PrintPlaces()
        {
            foreach (var item in Places)
            {
                Console.WriteLine($"The place {item.Number} is {item.Status}.");
            }
        }
    }
}

//Как пользователь хочу забронировать N мест после каждого бронирования

