﻿namespace Stadion.Models
{
    public class Place
    {
        public Place(int number, double price)
        {
            Number = number;
            Price = price;
        }

        public int Number { get; private set; }

        public double Price { get; private set; }

        public StatePlace Status { get; set; } = StatePlace.Free;
    }
}
