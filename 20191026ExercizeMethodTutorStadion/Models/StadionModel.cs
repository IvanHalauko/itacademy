﻿using System;
using System.Collections.Generic;

namespace Stadion.Models
{
    public class StadionModel
    {
        public StadionModel(Random random)
        {
            var baseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var chars = new char[10];
            for (int i = 0; i < 3; i++)
            {
                chars[i] = baseChars[random.Next(baseChars.Length - 1)];
            }

			//есть лист который мы инициализируем
            Name = new string(chars);

            Sectors = new List<Sector>();
			//можно обращаться к полю без создания объекта Shop в новом классе
			Shop.Name = "Какой-то магазин";

            for (int i = 3; i > 0; i--)
            {
                // Заполняем коллекцию
                Sectors.Add(new Sector(random));
            }
        }

        public string Name { get; set; }

        public List<Sector> Sectors { get; set; }

		//создали поле внутри стадион модел
		public Shop Shop;

    }
}
