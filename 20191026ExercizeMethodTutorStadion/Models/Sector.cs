﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stadion.Models
{
    public class Sector
    {
        public Sector(Random random)
        {
            var baseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var chars = new char[2];
            for (int i = 0; i < 2; i++)
            {
                chars[i] = baseChars[random.Next(baseChars.Length - 1)];
            }

            Name = new string(chars);
            Rows = new List<Row>();

            for (int i = 5; i > 0; i--)
            {
                // Заполняем коллекцию
                Rows.Add(new Row(random));
            }
        }

        public string Name { get; set; }

        public List<Row> Rows { get; set; }
    }
}
