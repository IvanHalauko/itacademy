﻿using Stadion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stadion
{
    public class Worker
    {
        private readonly StadionModel _stadion;

        public Worker()
        {
            _stadion = new StadionModel(new Random());
            ShowAllSectors();
        }

        public void ShowAllSectors()
        {
            foreach (var item in _stadion.Sectors)
            {
                Console.WriteLine($"Sector {item.Name};");
            }
        }

        public void ShowRowsInSectorWithFreePlaces(int sectorId)
        {
            var sector = _stadion.Sectors[sectorId];

            foreach (var item in sector.Rows)
            {
                item.PrintPlaces();
            }
        }

        public void ShowPlacesInRow(int sectorId, int rowId)
        {
            var sector = _stadion.Sectors[sectorId];
            var row = sector.Rows[rowId];

            row.PrintPlaces();
        }

        public void BookPlace(int sectorId, int rowId, int placeId)
        {
            var sector = _stadion.Sectors[sectorId];
            var row = GetRow(rowId, sector);

            row.Places[placeId].Status = StatePlace.Booked;

            row.PrintPlaces();
        }

        private Row GetRow(int number, Sector sector)
        {
            //foreach (var item in sector.Rows)
            //{
            //    if (item.Number == number)
            //    {
            //        return item;
            //    }
            //}

            return sector.Rows[number];
        }

		//метод оплаты всех забронированных мест
		public void PaidPlaces()
		{
			foreach(var sector in _stadion.Sectors)
			{
				foreach (var row in sector.Rows)
				{
					foreach (var place in row.Places)
					{
						if (place.Status==StatePlace.Booked)
						{
							place.Status = StatePlace.Paid;
							Console.WriteLine($"Sector");

						}
					}
					
					

				}
			}
		}


		public void PrintShops()
		{
			_stadion.Shop.PrintName();
		}

    }
}