﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flag
{ //пример работы с операторами
	class Program
	{
		static void Main(string[] args)
		{
			bool flag = true;
			int x = 5;

			Console.WriteLine($"Результат операции {x}+=3 {x+=3}");
			//int x = 5;

			Console.WriteLine($"Результат операции {x}-=4 {x -= 4}");


			Console.WriteLine($"Результат операции {x}*=2 {x *= 2}");

			Console.WriteLine($"Результат операции {x}/=2 {x /= 2}");

			Console.WriteLine($"Результат операции {x}%=2 {x %= 2}");

			Console.WriteLine($"Результат операции {flag}&= {flag & true}");


			Console.WriteLine($"Результат операции {flag}|= {flag |= true}");


			Console.WriteLine($"Результат операции {flag}^= {flag ^= true}");




		}
	}
}
