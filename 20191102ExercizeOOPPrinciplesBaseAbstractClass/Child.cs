﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191102ExercizeOOPPrinciplesBaseAbstractClass
{
	public class Child : BaseAbstractClass
	{
		public override string ChangeName(string name)
		{
			return $"New name {name}";

		}
	}
}
