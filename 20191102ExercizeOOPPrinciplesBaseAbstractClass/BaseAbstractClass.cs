﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191102ExercizeOOPPrinciplesBaseAbstractClass
{
	public abstract class BaseAbstractClass
	{
		public string Name { get; set; }

		public BaseAbstractClass()
		{
			Name = "DEF NAME";
		}

		public BaseAbstractClass(string name)
		{
			Name = name;
		}
		//public abstract string ChangeName(string name);

		public virtual string ChangeName (string name)
		{
			return name;
		}
	}
}
