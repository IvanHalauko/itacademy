﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using task01_NET;

namespace UnitTestTask01_NET
{
	[TestClass]
	public class UnitTest1
	{   /// <summary>
		///  Test check up method's EuclideanAlgorithm result of two numbers.
		/// </summary>
		[TestMethod]
		public void GivenEuclideanAlgorithm_WhenForTwoNumb_200_100_ThenIs100()
		{			
			Assert.AreEqual(100, AlgorithmGCD.EuclideanAlgorithm(200, 100, out double elapsedTime));
		}
		/// <summary>
		///  Test check up method's result of three numbers.
		/// </summary>
		[TestMethod]
		public void GivenEuclideanAlgorithm_WhenForThreeNumb_200_100_10_ThenIs10()
		{			
			Assert.AreEqual(10, AlgorithmGCD.EuclideanAlgorithm(200, 100,10, out double elapsedTime));
		}
		/// <summary>
		///  Test check up method's result of four numbers.
		/// </summary>
		[TestMethod]
		public void GivenEuclideanAlgorithm_WhenForFourNumb_200_100_10_5_ThenIs5()
		{
			Assert.AreEqual(5, AlgorithmGCD.EuclideanAlgorithm(200, 100, 10,5, out double elapsedTime));
		}
		/// <summary>
		///  Test check up method's result of two numbers using Stein's algorithm.
		/// </summary>
		[TestMethod]
		public void GivenSteinsAlgorithm_WhenForTwoNumb_15_5_ThenIs5()
		{		
			Assert.AreEqual(5, AlgorithmGCD.SteinsAlgorithm(15, 5, out double elapsedTime));
		}

		/// <summary>
		///  Test check up method's result of three numbers using Stein's algorithm.
		/// </summary>
		[TestMethod]
		public void GivenSteinsAlgorithm_WhenForThreeNumb_20_10_5_ThenIs5()
		{
			Assert.AreEqual(5, AlgorithmGCD.SteinsAlgorithm(20, 10,5, out double elapsedTime));
		}

		/// <summary>
		///  Test check up method's result of four numbers using Stein's algorithm.
		/// </summary>
		[TestMethod]
		public void GivenSteinsAlgorithm_WhenForThreeNumb_40_20_10_5_ThenIs5()
		{
			Assert.AreEqual(5, AlgorithmGCD.SteinsAlgorithm(40,20, 10, 5, out double elapsedTime));
		}



	}
}
