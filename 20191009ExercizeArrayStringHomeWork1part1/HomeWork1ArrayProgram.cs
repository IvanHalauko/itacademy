﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191009ExercizeArrayString
{
	class HomeWork1ArrayProgram
	{

		//создать двумерный массив размерностью 4х4 типа int
		//инициализировать как удобно
		//в цикле вычислить сумму всех элементов строки
		//вывести сумму каждого ряда на консоль
		//

		static void Main(string[] args)
		{
			int sizeArray = 4;
			int[,] twoDimensArray = new int[4, 4] { { 1, 4, 5, 9 }, { 1, 2, 3, 1 }, { 5, 7, 9, 2 }, { 3, 4, 7, 2 } };//Создать двумерный массив 3х3. Значения элементов 

			for (int rowIndex = 0; rowIndex < sizeArray; rowIndex++)
			{
				//int rowIndex = 0;
				int[] row = Enumerable.Range(0, twoDimensArray.GetUpperBound(1) + 1).Select(i => twoDimensArray[rowIndex, i]).ToArray(); //Выборка строки из двумерного массива и передача в одномерный массив
				int summElemArr = row.Sum();
				//var maxValue = row.Max(); //Поиск максимального элемента в двумерном массиве

				Console.Write($" массив:{rowIndex} элементы массива:");
				foreach (var element in row)
				{
					Console.Write(element);
					Console.Write(" ");
				}
				Console.WriteLine($"сумма элементов массива в строке: {summElemArr}");
			}
		}
	}
}
