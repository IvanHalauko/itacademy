﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;



//по нажатию скан проект начинает искать рядом с собой текстовые файлы
// будет 3 потока (один поток сканирует дирректорию, читает содержимое файлов и выводит на кран
// третий занимается интерфейсом
namespace _20191123ExercizeWinFormsTutor
{
	public partial class Form1 : Form
	{
		private readonly Thread _scanningThread;

		public Form1()
		{
			InitializeComponent();
			
		}

		//private void Form1_Load(object sender, EventArgs e)
		//{

		//}

		

		private void startButton_Click(object sender, EventArgs e)
		{
			ScanDirectory();

			_scanningThread = new Thread(ScanDirectory);
			

		}

		private void button2_Click(object sender, EventArgs e) //stopButton
		{

		}
		//
		private void ScanDirectory()
		{
			//получим где наш проект находится
			var currentDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ScanedFolder");

			//создаем лист уже добавленных файлов для ускорения работы приложения
			List<string> listExistedFiles = new List<string>();
			// приложение создает само папку
			//проверяем сущ эта папка, если нет, то создаем
			if (!Directory.Exists(currentDirectory))
			{
				Directory.CreateDirectory(currentDirectory);
			}

			//плохим считается писать тред и делать в нем бесконечный цикл
			//создаем цикл, который будет сканировать без остановки папку

			while (true)
			{
				//Нужно получить все файлы расширение которых только тхт
				var files = Directory.GetFiles(currentDirectory);

				foreach (var file in files)
				{
					if (Path.GetExtension(file).ToUpper().Equals(".TXT") && !listExistedFiles.Contains(Path.GetFileName(file)))

					{
						statusStrip1.Invoke(new Action(()=>
						{
							fileNameLabel.Text = Path.GetFileName(file);
						}));

					//построково читаем файл
					linesFile.Invoke(new Action(() =>
					{
						linesFile.Items.Clear();
					}));

						foreach (var line in File.ReadAllLines(file))
							{
							linesFile.Invoke(new Action(()=>
								{
									linesFile.Items.Add(line);
								}));
							}
						ThreadPool.QueueUserWorkItem(FileReader, file);
					}
				}

				Thread.Sleep(1000);
			}
			

	}
		//метод чтения файла
		private void FileReader(string obj)
		{
			string file = obj as string;

			linesFile.Invoke(new Action(() =>
			{
				linesFile.Items.Clear();
			}));

			foreach (var line in File.ReadAllLines(file))
			{
				linesFile.Invoke(new Action(() =>
				{
					linesFile.Items.Add(line);
				}));
			}
		}

		//для того что б потоки остановились при закрытии формы приложения нужно:
		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_scanningThread?.ThreadState!=ThreadState.Running)
			{
				_scanningThread?.Abort(); //? гарантирует что Аборт не будет вызван
			}
		}
	}
}
