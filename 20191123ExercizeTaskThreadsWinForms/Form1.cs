﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

            var thread = new Thread(MyCustomMethod);
            thread.Start();
            button1.Text = "Processing...";
            button1.Enabled = false;
        }

        private void MyCustomMethod()
        {
            for (int i = 0; i < 30; i++)
            {
                Thread.Sleep(300);
                listBox1.Invoke(new Action(() => 
                {
                    listBox1.Items.Add(i);
                }));
            }
            button1.Invoke(new Action(() =>
            {
                button1.Enabled = true;
                button1.Text = "Finished";
            }));
        }
    }
}
