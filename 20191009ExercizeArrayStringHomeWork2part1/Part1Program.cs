﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191009ExercizeArrayStringHomeWork2
{
	// Используя домашнее задание с предыдущего ДЗ (написать игру) написать следующее:
	// 1. (выполнено) Создать двухмерный массив любой размерностью[n, n] типа int
	// 2. (выполнено) Программа должна автоматически инициализировать все элементы массива значениями 
	// от 1 до 9 используя Random
	// 3. (выполнено) Далее в цикле нажимая клавиши WSAD производить смещение по элементам массива
	// 4. (выполнено) Если игрок попадает на ячейку с числом которое делится на 3 без остатка, то игрок проиграл
	// 5. (выполнено) Игра начинается с позиции 0:0 (в этой ячейке ни когда не числа кратного 3-ём (не выполнено))
	// 6. (выполнено) Игра считается успешно завершенной по достижению ячейки n:n(т.е.дальнего угла массива)
	// 7. (выполнено) По нажатию кнопки Esc игрок может завершить игру в любой момент.
	// 8. (выполнено) Раскраска игрового поля и информационных сообщений.

	class Program
	{
		static void Main(string[] args)
		{

			int sizeArr = 9;
			int[,] battleFieldArr = new int[sizeArr, sizeArr];

			Random rand = new Random(); //объявление переменной для генерации случайных чисел
			for (int i = 0; i < sizeArr; i++) //row ряды
			{
				for (int j = 0; j < sizeArr; j++) // colums колонки
				{
					battleFieldArr[i, j] = rand.Next(1, 9);
				}
			}


			//Цикл вывода на консоль игрового поля с числами (массива)
			for (int rowIndex = 0; rowIndex < sizeArr; rowIndex++)
			{
				int[] row = Enumerable.Range(0, battleFieldArr.GetUpperBound(1) + 1).
					Select(i => battleFieldArr[rowIndex, i]).ToArray(); //Выборка строки из двумерного массива и передача в одномерный массив
																		// Выделение желтым цветом элемента массива [0,0] 
																		// и зеленым элемента со значением кратным 3														
				Console.Write($" массив:{rowIndex} элементы массива:");

				for (int i = 0; i < row.Length; i++)
				{
					if (rowIndex == 0  && i==0 ) // если элемент массива и строка имеет индекс 0, 
												// то выполняем смену цвета шрифта на зеленый и выводим
												// значение этого элемента на консоль
					{
						Console.ForegroundColor = ConsoleColor.Green;
						Console.Write(row[i]);
						Console.Write(" ");
					}
					else
					{	//проверяем кратен ли элемент 3 и если да, то
						//выводим на консоль, если нет, то меняем цвет на белый и выводим на консоль
						if (row[i] % 3 == 0)
						{
							Console.ForegroundColor = ConsoleColor.Red;
						}
						else Console.ForegroundColor = ConsoleColor.White;
						Console.Write(row[i]);
						Console.Write(" ");
						//Console.ForegroundColor = ConsoleColor.Blue;
					}
				}
				Console.ForegroundColor = ConsoleColor.White;
				Console.WriteLine(" "); // конец строки одномерного массива и перенос на следующую			
			}


			int rawInd = 0;
			int colInd = 0;
			Console.WriteLine($"Начало пути: [{battleFieldArr[rawInd, colInd]}]");
			Console.WriteLine("Нажмите A/S/D/W/ESC");
			var dirOfTravel = Console.ReadKey();
			//Console.WriteLine(dirOfTravel);

			while (dirOfTravel.Key != ConsoleKey.Escape)
			{
				//for (; rawInd<sizeArr || colInd<sizeArr;)
				//{
					switch (dirOfTravel.Key)
					{
						 
						case ConsoleKey.W: //W + up Y++, row--
						--rawInd;
						
						if (rawInd<0)
						{
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Вы вышли за поле игры. Вы проиграли");
						return;
						}
						else
							Console.WriteLine($"Вы тут: [{battleFieldArr[rawInd, colInd]}]");							
						if (battleFieldArr[rawInd,colInd] %3 == 0)
						{
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Вы проиграли.Вы попали на бомбу");
							return;
						}							
							dirOfTravel = Console.ReadKey();
						break;

						case ConsoleKey.S: //S, down, Y--, row++
						++rawInd;
						if (colInd == sizeArr-1)
						{
							if (rawInd == sizeArr-1)
							{
								Console.ForegroundColor = ConsoleColor.DarkYellow;
								Console.WriteLine("Вы ВЫИГРАЛИ! ПОЗДРАВЛЯЕМ!");
								return;
							}
							
						}
						if (rawInd > sizeArr-1)
						{
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Вы вышли за поле игры. Вы проиграли");
							return;
						}
						else
							Console.WriteLine($"Вы тут: [{battleFieldArr[rawInd, colInd]}]");							
						if (battleFieldArr[rawInd, colInd] % 3 == 0)
						{
							Console.WriteLine("Вы проиграли.Вы попали на бомбу");
							return;
						}							
							dirOfTravel = Console.ReadKey();
							break;
						case ConsoleKey.A: //A, left, X--, col--.
						--colInd;

						if (colInd < 0)
						{
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Вы вышли за поле игры. Вы проиграли");
							return;
						}
						else
							Console.WriteLine($"Вы тут: [{battleFieldArr[rawInd, colInd]}]");
													
						if (battleFieldArr[rawInd, colInd] % 3 == 0)
						{
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Вы проиграли.Вы попали на бомбу");
							return;
						}
							dirOfTravel = Console.ReadKey();
							break;

						case ConsoleKey.D: //D, right, X++, col++.
						++colInd;
						if (rawInd == sizeArr-1)
						{
							if (colInd == sizeArr-1)
							{
								Console.ForegroundColor = ConsoleColor.DarkYellow;
								Console.WriteLine("Вы ВЫИГРАЛИ! ПОЗДРАВЛЯЕМ!");
								return;
							}							
						}
						if (colInd > sizeArr-1)
						{
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Вы вышли за поле игры. Вы проиграли");
							return;
						}
						else
							Console.WriteLine($"Вы тут: [{battleFieldArr[rawInd, colInd]}]");
		
						if (battleFieldArr[rawInd, colInd] % 3 == 0)
						{
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Вы проиграли.Вы попали на бомбу");
							return;
						}							
							dirOfTravel = Console.ReadKey();
							break;												
					}					
				//}				
			}
			Console.ForegroundColor = ConsoleColor.Blue;
			Console.WriteLine("Вы вышли из игры. Нажата клавиша ESC");				
		}
	}
}