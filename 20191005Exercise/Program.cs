﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191005Exercise
{
	class Program
	{
		

		static void Main(string[] args)
		{
			int x = 0;
			int y = 0;

			Console.WriteLine("Enter x:");
			//x=Console.ReadLine
			int.TryParse(Console.ReadLine(), out x);

			Console.WriteLine("Enter y:");
			//x=Console.ReadLine
			int.TryParse(Console.ReadLine(), out y);

			if (x >= 0 && y >= 0)
			{
				if (x == y)
				{
					Console.WriteLine("Значения равны");
				}
				else if (x > y)
				{
					Console.WriteLine("Значение X больше Y");
				}
				else if (x < y)
				{
					Console.WriteLine("Значение X меньше Y");
				}
			}
			else
			{
				Console.WriteLine("Значения меньше ноля");
			}


		}
	}
}
