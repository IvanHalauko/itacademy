﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorSwitch
{
	class Program
	{
		static void Main(string[] args)
		{

			int x = 0;
			int y = 0;
			//string s = "";

			Console.WriteLine("Support operations: +,-");
			Console.WriteLine("Enter value for X:");
			//x=Console.ReadLine
			int.TryParse(Console.ReadLine(), out x);

			Console.WriteLine("Enter value for Y:");
			int.TryParse(Console.ReadLine(), out y);

			Console.WriteLine("Вы хотите произвести расчет? Y/N");
			ConsoleKeyInfo button = Console.ReadKey();
			Console.WriteLine();
			while (button.Key == ConsoleKey.Y)
			{
				Console.WriteLine(" ");
				Console.WriteLine("Enter operation:");
				var operation = Console.ReadKey();
				Console.WriteLine($"Result:{Calculate(x, y, operation)}");
				//Console.ReadLine();

				Console.WriteLine("Вы хотите продолжить расчет? Esc/Y");
				Console.WriteLine(" ");
				ConsoleKeyInfo button1 = Console.ReadKey();
				if (button1.Key == ConsoleKey.Escape)
				{
					Console.WriteLine("Расчет окончен");
					break;
				}
				else if (button1.Key == ConsoleKey.Y)
				{
					Calculate(x, y, operation);
				}
				
			}
		}
		static int Calculate(int x, int y, ConsoleKeyInfo consoleKey)
		{
			//Console.WriteLine("Enter operation2:");
			//consoleKey = Console.ReadKey();
			{
				if (consoleKey.Key == ConsoleKey.OemPlus)
				{
					return x + y;
				}

				if (consoleKey.Key == ConsoleKey.OemMinus)
				{
					return x - y;
				}
				if (consoleKey.Key == ConsoleKey.Escape)
				{
					Console.WriteLine("Нажата кнопка ESC");
				}
				//Console.WriteLine("Ни одна из доступных операций не выбрана ESC");
							
			}
			Console.WriteLine("Нажмите ESC еще раз");
			return -1;
			
		}

	}
}
		
