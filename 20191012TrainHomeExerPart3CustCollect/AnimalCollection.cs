﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace _20191012TExerPart3CustomCollection
{
	class AnimalCollection : Collection<Animal>
	
	{	
		public void InsertItem(Animal item)

		{
			if (item != null)
			{
				base.Add(item);
				Console.WriteLine($"Вы добавили новое животное:{item.Name} {item.Voice}");			
			}
			else
			{
				Console.WriteLine("Вы ничего не добавили");
			}
		}

		//Удалить животное
		protected override void RemoveItem(int index)
		{
			Console.WriteLine($"Вы удалили {Items[index].Voice}");
			base.RemoveItem(index);
		}
		
		internal static void Display(AnimalCollection print)
		{
			Console.WriteLine();
			foreach (var item in print)
			{
				Console.WriteLine($"{item.Voice}");
			}
		}

		//internal static void DisplayQueue(Queue<AnimalCollection> collectionQueue)
		//{
		//	Console.WriteLine();
		//	foreach (var item in collectionQueue)
		//	{
		//		Console.WriteLine($"{item.Name}");
		//	}
		//}
	}
}
