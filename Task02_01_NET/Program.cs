﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02_01_NET
{
	//Задание 2.1. 
	//1) (выполнено) Разработать класс «вектор» для работы с трехмерными векторами.
	//2) (выполнено) Перегрузить для класса операции, допустимые для работы с трехмерными векторами.
	//3) Создать unit-тесты для тестирования разработанных методов.
	class Program
	{
		static void Main(string[] args)
		{
			Vector vector1 = new Vector(1, 1, 1);
			Vector vector2 = new Vector(2, 2, 2);

			var vector3 = vector1 + vector2;
			Console.WriteLine(vector3);

			vector3 = vector1 - vector2;
			Console.WriteLine(vector3);

			vector3 = vector1 * vector2;
			Console.WriteLine(vector3);
		}
	}
}
