﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02_01_NET
{
	public class Vector
	{
		private double x;
		private double y;
		private double z;
		/// <summary>
		/// vector properties
		/// </summary>
		public double X
		{
			get { return this.x; }
		}
		public double Y
		{
			get { return this.y; }
		}
		public double Z
		{
			get { return this.z; }
		}

		private const string THREE_COMPONENTS = "Array must cantaain exactly three components, (x,y,z)";
		/// <summary>
		/// The constructor initializes the tree-dimension vector
		/// </summary>
		/// <param name="x">The first vector dimension</param>
		/// <param name="y">The second vector dimension</param>
		/// <param name="z">The third vector dimension</param>
		public Vector(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		//Operator overloading
		/// <summary>
		/// The method overrides the mathimatical "plus" operation for working with vectors
		/// </summary>
		/// <param name="v1">first vector</param>
		/// <param name="v2">second vector</param>
		/// <returns>return vector result</returns>
		public static Vector operator +(Vector v1, Vector v2)
		{
			return new Vector(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
		}
		/// <summary>
		/// The method overrides the mathimatical "minus" operation for working with two vectors
		/// </summary>
		/// <param name="v1">first vector</param>
		/// <param name="v2">second vector</param>
		/// <returns>return vector result</returns>
		public static Vector operator -(Vector v1, Vector v2)
		{
			return new Vector(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
		}
		/// <summary>
		/// The method overrides the mathimatical "multiply" operation for working with vectors and scalar
		/// </summary>
		/// <param name="v1">first vector</param>
		/// <param name="k">scalar</param>
		/// <returns>returns vector result after multipling the vector by a scalar</returns>
		public static Vector operator *(Vector v1, double k)
		{
			return new Vector(v1.X * k, v1.Y * k, v1.Z * k);
		}
		/// <summary>
		/// The method overrides the mathimatical "multiply" operation for working with two vectors
		/// </summary>
		/// <param name="v1">first vector</param>
		/// <param name="v2">second vector</param>
		/// <returns>return vector result after multipling two vectors</returns>
		public static Vector operator *(Vector v1, Vector v2)
		{
			return new Vector(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z);
		}
		/// <summary>
		/// override method ToString for display tree-dimension vector
		/// </summary>
		/// <returns>string vector [x,y,z]</returns>
		public override string ToString()
		{
			return string.Format("[{0},{1},{2}]", x, y, z);
		}

		/// <summary>
		/// Comparring the properties of two vectors
		/// </summary>
		/// <param name="obj">vector object</param>
		/// <returns>Return result after compare</returns>
		public override bool Equals(object obj)
		{
			if (obj is Vector vector)
			{
				return ((this.X == vector.X) && (this.Y == vector.Y) && (this.Z == vector.Z));
			}
			return false;
		}

		/// <summary>
		/// Hash calculation
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return 12*X.GetHashCode()+Y.GetHashCode()+Z.GetHashCode();
		}



	}
}
