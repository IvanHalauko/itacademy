﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191109ExercizeCollectionDictionary
{//не закончено
	class Program
	{
		static void Main(string[] args)
		{
			var database = new DataBase();
			Dictionary<TypeKey,List<string>> cars =new Dictionary<TypeKey, List<string> > ();

			cars.Add(TypeKey.Bus, database.GetCarsByTypeKey(TypeKey.Bus));
			cars.Add(TypeKey.Car, database.GetCarsByTypeKey(TypeKey.Car));
			//cars.Add(TypeKey.Truck, database.GetCarsByTypeKey(TypeKey.Truck));

			//var t = cars[TypeKey.Truck];

			if (!cars.ContainsKey<TypeKey>)
			{
				cars.Add(TypeKey.Truck, database.GetCarsByTypeKey(TypeKey.Truck));
			}


			if (cars.TryGetValue(TypeKey.Truck,out var list))
				{
				list.ForEach(c => Console.WriteLine(c));

			}
		}
	}

	class MyComparator : IComparer<string>
	{
		public int Compare(string x, string y)
		{
			if (x.Length > y.Length)
				return -1;
			if (x.Length < y.Length)
				return 1;
			else
				return 0;
		}
	}



	class DataBase
	{
		public List<string> GetCarsByTypeKey(TypeKey type)
		{
			return new List<string>
			{
				$"{type} : The best car",
				$"{type} : The best car",
				$"{type} : The best car",
				$"{type} : The best car",
				$"{type} : The best car"
			};
		}
	}

	enum TypeKey
	{
		Car,
		Bus,
		Truck
	}
}
