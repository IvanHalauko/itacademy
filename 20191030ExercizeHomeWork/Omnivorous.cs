﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191030ExercizeHomeWork
{
	class Omnivorous : Animal
	{
		public Omnivorous(int numIdAnimal, string nameAnimal, string typeAnimal, string typeMeals, double weightAnimal)
			: base(numIdAnimal, nameAnimal, typeAnimal, typeMeals, weightAnimal)
		{

		}

		public override double CountMeals()
		{
			double amount = WeightAnimal * 0.8;
			return amount;
		}
		public override string ToString()
		{
			return base.ToString();
		}
	}
}
