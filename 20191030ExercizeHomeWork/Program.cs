﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//1. (выполнено) Построить три класса (базовый и 3 потомка), описывающих некоторых хищных 
//животных (один из потомков), всеядных(второй потомок) и травоядных (третий потомок).
//2. (выполнено) Описать в базовом классе абстрактный метод для расчета количества и типа пищи, необходимого
//для пропитания животного в зоопарке.
//3. (выполнено) Упорядочить всю последовательность животных по убыванию количества пищи.
//4. (выполнено) При совпадении значений. Вывести идентификатор животного, имя, тип и количество потребляемой
//пищи для всех элементов списка.

namespace _20191030ExercizeHomeWork
{
	class Program
	{
		static void Main(string[] args)
		{
			Animal wolf = new Predator(1, "Jora", "predator", "meat", 10);
			Animal fox = new Predator(1, "Laura", "predator", "meat", 10);
			Animal elephant = new Herbivorous(1, "Jor", "herbivorous", "greenHerb", 700);
			Animal horse = new Herbivorous(1, "Jak", "herbivorous", "greenHerb", 700);
			Animal pig = new Omnivorous(1, "Nuf", "omnivorous", "all", 200);
			
			List<Animal> dicAnimals = new List<Animal>();
			dicAnimals.Add(wolf);
			dicAnimals.Add(fox);
			dicAnimals.Add(elephant);
			dicAnimals.Add(horse);
			dicAnimals.Add(pig);
						
			ComparerAnimal<Animal> instanceComparer = new ComparerAnimal<Animal>();
			
			dicAnimals.Sort(instanceComparer);
			DisplayToConsole(dicAnimals);

			Console.WriteLine("Вывод, повторяющихся по колличеству еды животных");
			DisplayRepeatCountMeals(dicAnimals);
		}
		private static void DisplayRepeatCountMeals(List<Animal> dicAnimals)
		{
			var temp = dicAnimals[0].CountMeals();
			for (int i = 1; i < dicAnimals.Count; i++)
			{
				if (temp == dicAnimals[i].CountMeals())
				{
					Console.WriteLine(dicAnimals[i]);
				}
				else
				{
					temp = dicAnimals[i].CountMeals();
				}
			}
		}

		private static void DisplayToConsole(List<Animal> dicAnimals)
		{
			foreach (var item in dicAnimals)
			{
				Console.WriteLine(item);
			}
		}
	}
}
