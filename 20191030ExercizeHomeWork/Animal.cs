﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191030ExercizeHomeWork
{
	public abstract class Animal 
	{
		public int NumIdAnimal { get; set; }
		public string NameAnimal { get; set; }
		public string TypeAnimal { get; set; }
		public string TypeMeals { get; set; }
		public double WeightAnimal { get; set; }

		public Animal(int numIdAnimal, string nameAnimal, string typeAnimal, string typeMeals, double weightAnimal)
		{
			NumIdAnimal = numIdAnimal; NameAnimal = nameAnimal; TypeAnimal = typeAnimal;
			TypeMeals = typeMeals; WeightAnimal = weightAnimal;
		}

		//2. Описать в базовом классе абстрактный метод для расчета количества и типа пищи, необходимого
		//для пропитания животного в зоопарке.
		/// <summary>
		/// dfgdgdg
		/// </summary>
		/// <returns>Расчет колличес</returns>
		public abstract double CountMeals();
		
		public override string ToString()
		{
			return string.Format("{0};{1};{2};{3};", NumIdAnimal, NameAnimal, TypeAnimal, CountMeals());
		}
	}
}
