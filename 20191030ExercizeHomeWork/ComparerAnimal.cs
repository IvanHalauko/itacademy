﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20191030ExercizeHomeWork
{
	public class ComparerAnimal<T> : IComparer<T>
		where T : Animal
	{
		public int Compare(T x, T y)
		{
			
			if (x.CountMeals() > y.CountMeals())
				return -1;
			if (x.CountMeals() < y.CountMeals())
				return 1;
			else
				return 0;
		}
	}
}
