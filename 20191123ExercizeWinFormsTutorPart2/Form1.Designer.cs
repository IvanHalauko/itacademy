﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.startButton = new System.Windows.Forms.Button();
			this.stopButton = new System.Windows.Forms.Button();
			this.linesFile = new System.Windows.Forms.ListBox();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.fileNameLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.filesCollection = new System.Windows.Forms.ListBox();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// startButton
			// 
			this.startButton.Location = new System.Drawing.Point(898, 10);
			this.startButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.startButton.Name = "startButton";
			this.startButton.Size = new System.Drawing.Size(114, 39);
			this.startButton.TabIndex = 0;
			this.startButton.Text = "Start scanning";
			this.startButton.UseVisualStyleBackColor = true;
			this.startButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// stopButton
			// 
			this.stopButton.Enabled = false;
			this.stopButton.Location = new System.Drawing.Point(898, 54);
			this.stopButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.stopButton.Name = "stopButton";
			this.stopButton.Size = new System.Drawing.Size(114, 39);
			this.stopButton.TabIndex = 1;
			this.stopButton.Text = "Stop scanning";
			this.stopButton.UseVisualStyleBackColor = true;
			this.stopButton.Click += new System.EventHandler(this.StopButton_Click);
			// 
			// linesFile
			// 
			this.linesFile.FormattingEnabled = true;
			this.linesFile.Location = new System.Drawing.Point(9, 10);
			this.linesFile.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.linesFile.Name = "linesFile";
			this.linesFile.Size = new System.Drawing.Size(417, 342);
			this.linesFile.TabIndex = 2;
			// 
			// statusStrip1
			// 
			this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.fileNameLabel});
			this.statusStrip1.Location = new System.Drawing.Point(0, 388);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
			this.statusStrip1.Size = new System.Drawing.Size(1021, 22);
			this.statusStrip1.TabIndex = 3;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(53, 17);
			this.toolStripStatusLabel1.Text = "Last file: ";
			// 
			// fileNameLabel
			// 
			this.fileNameLabel.Name = "fileNameLabel";
			this.fileNameLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// filesCollection
			// 
			this.filesCollection.FormattingEnabled = true;
			this.filesCollection.Location = new System.Drawing.Point(439, 10);
			this.filesCollection.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.filesCollection.Name = "filesCollection";
			this.filesCollection.Size = new System.Drawing.Size(443, 342);
			this.filesCollection.TabIndex = 4;
			this.filesCollection.SelectedIndexChanged += new System.EventHandler(this.FilesCollection_SelectedIndexChanged);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1021, 410);
			this.Controls.Add(this.filesCollection);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.linesFile);
			this.Controls.Add(this.stopButton);
			this.Controls.Add(this.startButton);
			this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Name = "Form1";
			this.Text = "Form1";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.ListBox linesFile;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel fileNameLabel;
        private System.Windows.Forms.ListBox filesCollection;
    }
}

