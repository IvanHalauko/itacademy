﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        private Thread _scanningThread;

        public Form1()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = false;
            stopButton.Enabled = true;

            filesCollection.Items.Clear();

            _scanningThread = new Thread(ScanDirrectory);
            _scanningThread.Start();
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = true;
            stopButton.Enabled = false;

            _scanningThread.Abort();
        }

        private void ScanDirrectory()
        {
            var currentDirrectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ScanedFolder");
            List<string> listExistedFiles = new List<string>();

            if (!Directory.Exists(currentDirrectory))
            {
                Directory.CreateDirectory(currentDirrectory);
            }

            while (true)
            {
                var files = Directory.GetFiles(currentDirrectory);
                
                foreach (var file in files)
                {
                    if (Path.GetExtension(file).ToUpper().Equals(".TXT") && !listExistedFiles.Contains(Path.GetFileName(file)))
                    {
                        listExistedFiles.Add(Path.GetFileName(file));

                        filesCollection.Invoke(new Action(() =>
                        {
                            filesCollection.Items.Add(file);
                        }));

                        statusStrip1.Invoke(new Action(() =>
                        {
                            fileNameLabel.Text = Path.GetFileName(file);
                        }));
                    }
                }

                Thread.Sleep(1000);
            }
        }

        private void FileReader(object obj)
        {
            string file = obj as string;

            linesFile.Invoke(new Action(() =>
            {
                linesFile.Items.Clear();
            }));

            foreach (var line in File.ReadAllLines(file))
            {
                linesFile.Invoke(new Action(() =>
                {
                    linesFile.Items.Add(line);
                }));
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_scanningThread?.ThreadState != ThreadState.Running)
            {
                _scanningThread?.Abort();
            }
        }

        private void FilesCollection_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedFile = filesCollection.SelectedItem.ToString();

            ThreadPool.QueueUserWorkItem(FileReader, selectedFile);
        }
    }
}
