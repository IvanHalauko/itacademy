﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Задание 2.2.
//Разработать класс «многочлен» для работы с многочленами от одной переменной.
//Перегрузить для класса операции, допустимые для работы с многочленами.
//Создать unit-тесты для тестирования разработанных методов.

namespace Task02_02_NET
{
	class Program
	{
		static void Main(string[] args)
		{
			double[] koefPolin1 = { 1, 2, 3 };
			Polynomial p1 = new Polynomial(koefPolin1);

			double[] koefPolin2 = { 1, 1, 1 };
			Polynomial p2 = new Polynomial(koefPolin2);

			var p3 = p2 + p1;
			Console.WriteLine(p3);

			//var p4 = p2 - p1;


		}
	}
}
