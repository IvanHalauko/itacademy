﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02_02_NET
{
	public class Polynomial
	{
		
		public double[] СoefPolynom { get; set; }
		public double[] СoefPolynomDividend { get; set; }
		public double[] CoefPolynomDivisor { get; set; }
		public double[] CoefPolynomQuotient { get; set; }
		public double[] CoefPolynomRemainder { get; set; }	


		public Polynomial(double[] сoefPolynom)
		{
			СoefPolynom = сoefPolynom;
		}

		/// <summary>
		/// The method overrides the mathimatical "plus" operation for working with two polynomials
		/// </summary>
		/// <param name="firstPolynom"> the first polynomial</param>
		/// <param name="secondPolynom">the second polynomial</param>
		/// <returns>return polynomial's result</returns>
		public static Polynomial operator +(Polynomial firstPolynom, Polynomial secondPolynom)
		{
			int degreePolynomResult=0;
			if (firstPolynom.СoefPolynom.Length>secondPolynom.СoefPolynom.Length)
			{
				degreePolynomResult = firstPolynom.СoefPolynom.Length;
			}
			else if (firstPolynom.СoefPolynom.Length <= secondPolynom.СoefPolynom.Length)
			{
				degreePolynomResult = firstPolynom.СoefPolynom.Length;
			}
			double[] koefPolynomResult = new double[degreePolynomResult];

			Polynomial PolynomResult = new Polynomial(koefPolynomResult);

			for (int i = 0; i < degreePolynomResult; i++)
			{
				PolynomResult.СoefPolynom[i] = firstPolynom.СoefPolynom[i] + secondPolynom.СoefPolynom[i];
			}

			return PolynomResult;
		}

		/// <summary>
		/// The method overrides the mathimatical "minus" operation for working with two polynomials
		/// </summary>
		/// <param name="firstPolynom"> the first polynomial</param>
		/// <param name="secondPolynom">the second polynomial</param>
		/// <returns>return polynomial's result</returns>
		public static Polynomial operator -(Polynomial firstPolynom, Polynomial secondPolynom)
		{
			int degreePolynomResult = 0;
			if (firstPolynom.СoefPolynom.Length > secondPolynom.СoefPolynom.Length)
			{
				degreePolynomResult = firstPolynom.СoefPolynom.Length;
			}
			else if (firstPolynom.СoefPolynom.Length <= secondPolynom.СoefPolynom.Length)
			{
				degreePolynomResult = firstPolynom.СoefPolynom.Length;
			}

			double[] koefPolynomResult = new double[degreePolynomResult];

			Polynomial PolynomResult = new Polynomial(koefPolynomResult);

			for (int i = 0; i < degreePolynomResult; i++)
			{
				PolynomResult.СoefPolynom[i] = firstPolynom.СoefPolynom[i] - secondPolynom.СoefPolynom[i];
			}

			return PolynomResult;
		}

		/// <summary>
		/// The method overrides the mathimatical "multiply" operation for working with two polynomials
		/// </summary>
		/// <param name="firstPolynom"> the first polynomial</param>
		/// <param name="secondPolynom">the second polynomial</param>
		/// <returns>return polynomial's result</returns>
		public static Polynomial operator *(Polynomial firstPolynom, Polynomial secondPolynom)
		{
			int degreePolynomResult = 0;
			if (firstPolynom.СoefPolynom.Length > secondPolynom.СoefPolynom.Length)
			{
				degreePolynomResult = firstPolynom.СoefPolynom.Length;
			}
			else if (firstPolynom.СoefPolynom.Length <= secondPolynom.СoefPolynom.Length)
			{
				degreePolynomResult = firstPolynom.СoefPolynom.Length;
			}

			double[] сoefPolynomResult = new double[firstPolynom.СoefPolynom.Length + secondPolynom.СoefPolynom.Length - 1];
			Polynomial PolynomResult = new Polynomial(сoefPolynomResult);
			for (int i = 0; i < firstPolynom.СoefPolynom.Length; ++i)
			{
				for (int j = 0; j < secondPolynom.СoefPolynom.Length; ++j)
				{
					сoefPolynomResult[i + j] += secondPolynom.СoefPolynom[i] * firstPolynom.СoefPolynom[j];
				}
			}
			return PolynomResult;
		}

		public Polynomial(double[] сoefPolynomDividend, double[] сoefPolynomDivisor, double[] сoefPolynomQuotient, double[] сoefPolynomRemainder)
		{
			СoefPolynomDividend = сoefPolynomDividend;
			CoefPolynomDivisor = сoefPolynomDivisor;
			CoefPolynomQuotient = сoefPolynomQuotient;
			CoefPolynomRemainder = сoefPolynomRemainder;
		}

		public static Polynomial operator /(Polynomial dividend, Polynomial divisor, Polynomial quotient, Polynomial remainder)
		{
			double[] coefPolynomDivided = dividend.СoefPolynom;
			double[] coefPolynomDivisor = divisor.СoefPolynom;
			double[] coefPolynomQuotient = new double[coefPolynomDivided.Length-coefPolynomDivisor.Length+1];
			double[] coefPolynomRemainder = remainder.CoefPolynomRemainder;

			if ((dividend.СoefPolynom[coefPolynomDivided.Length] == 0)| (divisor.СoefPolynom [coefPolynomDivisor.Length]== 0))
			{
				throw new ArithmeticException("Старший член многочлена делимого не может быть 0");
			}

			for (int i = 0; i < coefPolynomQuotient.Length; i++)
			{
				double coeff = coefPolynomRemainder[coefPolynomRemainder.Length - i - 1] / coefPolynomDivisor[coefPolynomDivisor.Length];
				coefPolynomQuotient[coefPolynomQuotient.Length - i - 1] = coeff;

				for (int j = 0; j < coefPolynomDivisor.Length; j++)
				{
					coefPolynomRemainder[coefPolynomRemainder.Length - i - j - 1] -= coeff * coefPolynomDivisor[coefPolynomDivisor.Length - j - 1];
				}

			}




		}




		}
}
